﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AnimationPooler : MonoBehaviour {

    public string Message;
    public Text Output = null;

    public virtual void OnEnable() {
       // if(!Output)
        Output.text = Message;
        Invoke("PullThis", 1.25f);
          
    }


    public void PullThis()
    {
        //Output.color = Color.white;
        ObjectPool.instance.PoolObject(this.gameObject);


    }
}
