﻿using UnityEngine;
using System.Collections;

public class Coins : PowerUps {

    /*public override void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.tag == "Mage")
        {
            ExecutePowerUP();
        }
    }*/
    public override void ExecutePowerUP()
    {
        AudioSource.PlayClipAtPoint(Upsound, this.transform.position);
        GameManager.instance.CoinText.GetComponent<Animation>().Play("CoinTextGlow");
        GameManager.instance.CurrentCoins++;
        //GameManager.instance.TimerSlider.value += 0.08f;
    }
}
