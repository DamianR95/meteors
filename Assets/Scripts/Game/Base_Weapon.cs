﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base_Weapon : MonoBehaviour {


	public MageBehaviour MB;
	int AtkPoints;
	int TravelSpeedPoints;
	int GrowSpeedPoints;

	public float AtkMultiplier;
	protected float TravelMultiplier;
	protected float GrowMultiplier;

	void UpdateStats(){
	
		AtkMultiplier = Mathf.Lerp (1f, 2f, AtkPoints / 10);
		TravelMultiplier = Mathf.Lerp (1f, 2f, TravelSpeedPoints / 10);
		GrowMultiplier = Mathf.Lerp (1f, 1.5f, GrowSpeedPoints / 10);
	
	}

	void SkillAdd(){
	}



	// Use this for initialization
	public virtual void Start () {

		MB = this.GetComponent<MageBehaviour>();
		
	}
	
	// Update is called once per frame
	void Update () {



	}
}
