﻿using UnityEngine;
using System.Collections;

public class AccelerometerInput : MonoBehaviour {

#if UNITY_ANDROID

    Vector3 minScreenBounds;
    Vector3 maxScreenBounds; 

    PlasmaShooter RS;
    GravityShooter GS;

    void Start() {
        RS = GameManager.instance.PlayerRef.gameObject.GetComponent<PlasmaShooter>();
        GS = GameManager.instance.PlayerRef.gameObject.GetComponent<GravityShooter>();
        minScreenBounds = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        maxScreenBounds = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
    }


    // Update is called once per frame
    void Update () {

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minScreenBounds.x + 1, maxScreenBounds.x - 1),
        transform.position.y, transform.position.z);


        if (!Application.isMobilePlatform)
            return;

        if (RS.ChargingPlasma)
            return;
        if (GS.ChargingGravity)
            return;
       // if (Input.acceleration.x < 0.006f)
         //   return;


        if (GameManager.instance._currentState == GameManager.GameState.InPlay)
            transform.Translate(Input.acceleration.x,0, 0);
    
    }

 

#endif
}
