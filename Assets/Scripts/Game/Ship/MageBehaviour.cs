﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MageBehaviour : MonoBehaviour {


	public float fuel = 100;
	public Slider FuelSlider;
    public AttackType Weapon;

    public GameObject body;
	private bool vulnerable = true;
	private float fVulnTimeLeft = 0.0f;
	public AudioClip hurt;

    //public Material mat = null;
    public SpriteRenderer[] sr;
	private Color[] _lerpingColors = { Color.red, Color.blue };

    void PlayAnimation(string N_Animation) {

        body.GetComponent<Animation>().Play(N_Animation);
    
    }


	void Start(){
		//this.mat = this.GetComponent<Renderer> ().material;
        sr = GetComponentsInChildren<SpriteRenderer>();
	}


	public virtual void  OnTriggerEnter2D(Collider2D other) {
		//Destroy(other.gameObject);
		if (other.tag == "Meteor") {

            //other.GetComponent<MeteorBehaviour>().KillThis();
			//ObjectPool.instance.PoolObject (other.gameObject);
			//GameManager.instance.LosingLife (1,true);
			ReceiveDamage();
			AudioSource.PlayClipAtPoint (hurt, this.transform.position);

		}
        if (other.tag == "Aircraft")
        {
			ReceiveDamage();
           // GameManager.instance.LosingLife(1, true);
            AudioSource.PlayClipAtPoint(hurt, this.transform.position);
            //ObjectPool.instance.PoolObject(other.gameObject);
            //other.GetComponent<MeteorBehaviour>().KillThis();
			SpawnManager.instance.SpecialSpawn("ExplosionMobile", other.transform.position);

        }


		if (other.tag == "BossMeteor") {
			ReceiveDamage();
			//GameManager.instance.LosingLife (3,true);
           // ObjectPool.instance.PoolObject(other.gameObject);
            //other.GetComponent<MeteorBehaviour>().KillThis();
			AudioSource.PlayClipAtPoint (hurt, this.transform.position);
            
			SpawnManager.instance.SpecialSpawn("ExplosionMobile", other.transform.position);

		}
		if (other.tag == "PowerUp") {
			ObjectPool.instance.PoolObject (other.gameObject);
			if(Tutorial_Behaviour.isTutorial())
				Tutorial_Behaviour.instance.Continue("Grabed");
		}

	}



	void Update(){
		
		if (!canReceiveDamage ()) {
			fVulnTimeLeft -= Time.deltaTime;
			if(fVulnTimeLeft <= 0){
				vulnerable = true;
				fVulnTimeLeft = 2.0f;
			}		
		}
    }


	void FixedUpdate(){
		
		if (fuel > 0) {
			//fuel -= 0.03f;
			
		}
        FuelSlider.value = fuel;
        fuel = Mathf.Clamp(fuel, 0f, 100f);
    }

/*	void Shoot(){
	
		switch (Weapon){

		case AttackType.Laser:
			LaserEm ();
			break;
		case AttackType.Plasma:
			//PlasmaEm ();
			break;
		default:
			break;

		}
	
	}*/

   /* public void LaserEm() {
        AudioSource.PlayClipAtPoint(lasersound, this.transform.position);
        SpawnManager.instance.SpecialSpawn("LaserBullet", transform.position);
    }*/


    public bool ReceiveDamage(){
		if (canReceiveDamage ()) {
			fuel -= 33f;
			vulnerable = false;
			StartCoroutine(Blink ());
			fVulnTimeLeft = 2.0f;

			return true;
		}
		return false;
	}

	public bool canReceiveDamage(){
		return vulnerable;
	}

	public IEnumerator  Blink(){
		float time = 2f;
		float elapsed = 0f;
		int index = 0;

		float interval = 0.1f;
		while (elapsed < time) {
			//mat.color = _lerpingColors[index % 2];
            foreach (SpriteRenderer i in sr)
            {
                i.color = _lerpingColors[index % 2];
            }
			index++;
			elapsed += Time.deltaTime + interval;
			yield return new WaitForSeconds(interval);
		}
        foreach (SpriteRenderer i in sr)
        {
            i.color = Color.white;
        }
		//mat.color = Color.white;
	}



}
