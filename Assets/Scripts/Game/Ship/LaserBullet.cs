﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBullet : Bullet {

	public LaserShooter LS;


	// Use this for initialization
	void Start () {
	}

	protected override void OnEnable(){

        if (!LS)        
            LS = GameManager.instance.PlayerRef.gameObject.GetComponent<LaserShooter>();
      
            Damage = LS.Damage * LS.AtkMultiplier;
        WhereToGo ();
	}

	// Update is called once per frame
	protected	override void Update () {

		base.Update();

		transform.position = transform.position + thrust * direction * Time.deltaTime;

	}
}
