﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaBullet : Bullet
{

	//public float minPlasmaSize;
	//public Vector3 currentSize;
	//public float maxPlasmaSize;
	public float charge;
	public float multiplyaux;
	Vector3 auxvec;

    // Use this for initialization
    protected override void OnEnable()
    {
        base.OnEnable();


    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();


        if (direction != Vector3.zero)
        {
            transform.position = transform.position + thrust * direction * Time.deltaTime;


			auxvec.x = Mathf.Lerp (0.02f, 2f, (Damage / multiplyaux) / 10);
			auxvec.y = Mathf.Lerp (0.02f, 2f, (Damage / multiplyaux) / 10);
			auxvec.z = transform.localScale.z;
			transform.localScale = auxvec;


        }
    }



}