﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaShooter : Base_Weapon {



	float Base_Damage;
    public float Damage;
    public GameObject ChargingPlasma;

	public float Charge;
	public float Max_Charge;
	//Grow Ratio
	public float growSpeed;
	//Size
    public float minPlasmaSize;
    public Vector3 currentSize;
    public float maxPlasmaSize;
	//Speed
    private float travelSpeed;
    public float mintravelSpeed;
    public float maxtravelSpeed;

    public GameObject sourceWeapon;

    PlasmaBullet RB;
    //float timeaux;



    public void PlasmaEm() {

        if (MB.fuel <= 0)
            return;

        if (ChargingPlasma)
            return;

        ChargingPlasma = ObjectPool.instance.GetObjectForType("PlasmaBullet");

        ChargingPlasma.transform.position = sourceWeapon.transform.position;
        ChargingPlasma.transform.parent = SpawnManager.instance.goInScene.transform;
        RB = ChargingPlasma.GetComponent<PlasmaBullet>();
    }

#if UNITY_EDITOR
   void OnMouseDown() {
        if (GameManager.instance._currentState == GameManager.GameState.InPlay
       && MB.Weapon == AttackType.Plasma)
            PlasmaEm();

    }
#endif

    void ReleaseMouse() {
        Vector3 pos = Input.mousePosition;

        pos = Camera.main.ScreenToWorldPoint(pos);


        Release(pos);

    }


    public void Release(Vector3 pos) {


        pos.z = ChargingPlasma.transform.position.z;
        pos = (-transform.position + pos).normalized;
        Debug.Log(pos);

        RB.direction = pos;
        RB.thrust = travelSpeed;

        Charge = 1;
        currentSize = new Vector3(minPlasmaSize, minPlasmaSize, 1f);
        ChargingPlasma = null;
        RB = null;
    }

	public override void Start() {
		base.Start ();
		Base_Damage = Damage;

    }

    void Update() {

        if (ChargingPlasma) {

			if (Charge < Max_Charge && MB.fuel > 0) {

                Charge += (0.09f * growSpeed);
                Charge = Mathf.Clamp(Charge, 1, 10);

                currentSize += new Vector3(Time.deltaTime * growSpeed, Time.deltaTime * growSpeed, 0f);
                currentSize.x = Mathf.Clamp(currentSize.x, minPlasmaSize, maxPlasmaSize);
                currentSize.y = Mathf.Clamp(currentSize.y, minPlasmaSize, maxPlasmaSize);

                travelSpeed = Mathf.Lerp(maxtravelSpeed, mintravelSpeed, currentSize.x / maxPlasmaSize);
            }


            MB.fuel -= 0.1f;

			if (Charge == Max_Charge)
                Charge *= 2;

			RB.Damage = Damage * Charge * AtkMultiplier;
			RB.multiplyaux = AtkMultiplier;
            RB.charge = Charge;

            ChargingPlasma.transform.localScale = currentSize;
        }

#if UNITY_EDITOR
        if (Input.GetMouseButtonUp(0) && ChargingPlasma != null) {
            ReleaseMouse();
        }
#endif
    }
}
