﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{

    public float thrust;

	public AttackType AtkType;
	public float BaseDamage;
	public float Damage;

	public Vector3 direction;

	public Vector3 target;


	protected virtual void OnEnable()
    {
		Damage = BaseDamage;
		WhereToGo();
    }
    protected virtual void Update()
    {
		
        CheckIfNeedToDie();
    }



	protected void WhereToGo() {
		
		switch (AtkType) {

		case AttackType.Laser:
			direction = transform.right;
			break;
		default:
			direction = Vector3.zero;
			break;

		}
	}


	public virtual void SelfDestruct(){
		direction = Vector3.zero;
		Damage = BaseDamage;
		ObjectPool.instance.PoolObject(this.gameObject);

	}

    public virtual void CheckIfNeedToDie()
    {
        Vector3 a = Camera.main.WorldToViewportPoint(transform.position);

		if (a.y > 1.5 || a.y < -0.3 || a.x < -0.3 || a.x > 1.3)
        {
			SelfDestruct ();
		}
      


    }
}
