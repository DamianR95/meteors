﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityBullet : Bullet {


    //public float minPlasmaSize;
    //public Vector3 currentSize;
    //public float maxPlasmaSize;
    public float charge;
    public bool isslowed = false;

    //public Sprite[] ObjectSprite;
    //protected SpriteRenderer sr;

    public List<MeteorBehaviour> currentTrapped = new List<MeteorBehaviour>();
    public MeteorBehaviour MB;
    Vector3 auxvec;

    // Use this for initialization
    protected override void OnEnable() {
        base.OnEnable();
        //if (sr == null)
        // sr = GetComponent<SpriteRenderer>();
        isslowed = false;
        //sr.sprite = ObjectSprite[0];
    }


    public virtual void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Meteor" || other.tag == "Aircraft" || other.tag == "BossMeteor") {

            //sr.sprite = ObjectSprite[1];

            currentTrapped.Add(MB = other.GetComponent<MeteorBehaviour>());
            Vector2 aux = new Vector2(transform.position.x, transform.position.y);

            if (MB) {
                MB.auxVector = aux;
                MB.currentspeed /= 3;
            }

            if (!isslowed) {
                isslowed = true;
                // thrust /= 10;

                Invoke("SelfDestruct", 2f);
            }
        }
    }


    public virtual void OnTriggerExit2D(Collider2D other) {

        if (other.tag == "Meteor" || other.tag == "Aircraft" || other.tag == "BossMeteor") {

            //sr.sprite = ObjectSprite[1];

            currentTrapped.Remove(MB = other.GetComponent<MeteorBehaviour>());
            Vector2 aux = new Vector2(transform.position.x, transform.position.y);

            if (MB) {
                MB.auxVector = MB.whereToLook;
                MB.currentspeed *= 3;
            }

           
        }



    }

    public override void SelfDestruct() {
        foreach (MeteorBehaviour gObject in currentTrapped) {

            gObject.auxVector = gObject.whereToLook;
            gObject.currentspeed /= 3;
        }
        currentTrapped.Clear();
        MB = null;
        direction = Vector3.zero;
        Damage = BaseDamage;
        CancelInvoke("SelfDestruct");
        ObjectPool.instance.PoolObject(this.gameObject);
        //sr.sprite = ObjectSprite[0];

    }


    // Update is called once per frame
    protected override void Update() {
        base.Update();



        if (direction != Vector3.zero) {
            transform.position = transform.position + thrust * direction * Time.deltaTime;


            /*auxvec.x = Mathf.Lerp (0.02f, 2f, Damage / 10);
			auxvec.y = Mathf.Lerp (0.02f, 2f, Damage / 10);
			auxvec.z = transform.localScale.z;
			transform.localScale = auxvec;*/


        }
    }

}
