﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityShooter : Base_Weapon {



	float Base_Damage;
    public float Damage;
    public GameObject ChargingGravity;


    public float Charge;
	public float Max_Charge;

  //Size
    public float minGravitySize;
    public Vector3 currentSize;
    public float maxGravitySize;

 //Speed
    private float travelSpeed;
    public float mintravelSpeed;
    public float maxtravelSpeed;

//Grow Ratio
    public float chargeSpeed;
    public float growSpeed;


    Vector3 Distance;

    public Vector3 InputPos;

    public GameObject sourceWeapon;

    GravityBullet GB;

    public void GravityEm() {

        if (MB.fuel <= 0)
            return;

        if (ChargingGravity)
            return;

        ChargingGravity = ObjectPool.instance.GetObjectForType("GravityBullet");

        Charge = 0f;
        InputPos = Input.mousePosition;

        ChargingGravity.transform.position = sourceWeapon.transform.position;
        ChargingGravity.transform.parent = SpawnManager.instance.goInScene.transform;
        GB = ChargingGravity.GetComponent<GravityBullet>();
    }

    #if UNITY_EDITOR
    void OnMouseDown() {

        if (GameManager.instance._currentState == GameManager.GameState.InPlay
       && MB.Weapon == AttackType.Gravity)
            GravityEm();

    }
    #endif

    void ReleaseMouse() {
        Vector3 pos = Input.mousePosition;

        pos = Camera.main.ScreenToWorldPoint(pos);


        Release(pos);

    }


    public void Release(Vector3 pos) {

        if (!ChargingGravity) return;

        pos.z = ChargingGravity.transform.position.z;
        pos = (-transform.position + pos).normalized;
        //Debug.Log(pos);

        GB.direction = pos;
        GB.thrust = travelSpeed;

        ResetBullet();
  
    }

    void ResetBullet() {
        ChargingGravity = null;
        GB = null;


        Charge = 1;
        currentSize = new Vector3(minGravitySize, minGravitySize, 1f);

    }


    void ChargingMouse() {
        Distance = Input.mousePosition - InputPos;
        Distance.Normalize();

        Charge += (Distance.magnitude * growSpeed * 7);
        InputPos = Input.mousePosition;

    }
    public void ChargingMobile(Vector2 mobilePos) {
        Distance = new Vector3(mobilePos.x, mobilePos.y);

        Distance.Normalize();
        Charge += (Distance.magnitude * growSpeed * 7);
       // Debug.Log("Charge:" + Charge);
    }

	public override void Start() {
		base.Start ();
		Base_Damage = Damage;

	}



    void Update() {

        if (ChargingGravity) {

			if (Charge < Max_Charge && MB.fuel > 0) {


                #if UNITY_EDITOR
                ChargingMouse();
                #endif


                Charge = Mathf.Clamp(Charge, 1, 15);
                //	Debug.Log (Distance);
                currentSize += new Vector3(Distance.magnitude * growSpeed, Distance.magnitude * growSpeed, 0f);
                currentSize.x = Mathf.Clamp(currentSize.x, minGravitySize, maxGravitySize);
                currentSize.y = Mathf.Clamp(currentSize.y, minGravitySize, maxGravitySize);
                Distance = Vector3.zero;
                travelSpeed = Mathf.Lerp(maxtravelSpeed, mintravelSpeed, currentSize.x / maxGravitySize);
            }




            //MB.fuel -= 0.1f;

            //if (Charge == 15)
            //Charge *= 2;

			GB.Damage = Damage * Charge * AtkMultiplier;

            //GB.charge = Charge;

            ChargingGravity.transform.localScale = currentSize;

            if (GB.isslowed)
                ResetBullet();


        }

        	#if UNITY_EDITOR
        if (Input.GetMouseButtonUp(0) && ChargingGravity != null) {
            ReleaseMouse();
        }
        	#endif
    }




}
