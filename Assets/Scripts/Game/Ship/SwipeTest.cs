﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeTest : MonoBehaviour {

	Vector3 Pos;
	float chargeCounter;

	void OnMouseDown(){
		chargeCounter = 0;
		Pos = Input.mousePosition;
	}

	void OnMouseDrag(){
		Vector3 Distance = Input.mousePosition - Pos;
		Distance.Normalize ();
		//Debug.Log ("Mouse Moved: " + Distance.magnitude);

		chargeCounter += Distance.magnitude;
		Pos = Input.mousePosition;
	
	}

	void OnMouseUp(){
		Debug.Log (chargeCounter);
	}
}
