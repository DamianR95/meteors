﻿using UnityEngine;
using System.Collections;

public class MoveMage : MonoBehaviour {


	private Vector3 screenPoint;
	private Vector3 offset;

    PlasmaShooter PS;
	GravityShooter GS;

	#if UNITY_EDITOR
	/*void OnMouseDown()
	{
		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
		
	}
	
	void OnMouseDrag()
	{
		if(GameManager.instance._currentState == GameManager.GameState.InPlay){
			Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
			
			Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
			Vector3 ourPos = transform.position;
			
			ourPos.x = curPosition.x;
			transform.parent.position = ourPos;
			if(Tutorial_Behaviour.isTutorial())
				Tutorial_Behaviour.instance.Continue("Moved");
		}
	}*/


    void Clicked(){

        screenPoint = Camera.main.WorldToScreenPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, gameObject.transform.position.z));
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }


    void Dragged()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        Vector3 ourPos = transform.position;

        ourPos.x = curPosition.x;
        transform.parent.position = ourPos;

        if (!Dynamic_Tutorial.instance.step1)
            Dynamic_Tutorial.instance.Step1();



      /*  if (Tutorial_Behaviour.isTutorial())
            Tutorial_Behaviour.instance.Continue("Moved");*/
    }


    void Update()
    {
		if (GameManager.instance._currentState == GameManager.GameState.InPlay )
        {
			if (PS.ChargingPlasma == null && GS.ChargingGravity == null) {
				if (Input.GetMouseButtonDown (0))
					Clicked ();
				if (Input.GetMouseButton (0))
					Dragged ();
			}

           // transform.Translate(Input.acceleration.x, 0, 0);


        }
    }
#endif



    public void moveToX(float x){
		Vector3 ourPos = transform.position;
		ourPos.x = x;
		transform.parent.position = ourPos;
		//if tutorial, once
		if(Tutorial_Behaviour.isTutorial())
			Tutorial_Behaviour.instance.Continue("Moved");
	}



	// Use this for initialization
	void Start () {

        PS = transform.parent.GetComponent<PlasmaShooter>();
		GS = transform.parent.GetComponent<GravityShooter> ();

    }
	
	// Update is called once per frame
	
}
