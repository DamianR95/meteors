﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningShooter : Base_Weapon {


	float Base_Damage;
	public float Damage;


	public float CalculateDistance(Vector3 Enemypos) {

		float min = 0.5f;
		float max = 2.4f;

		float radius = 20f;


		Vector3 magePosition = this.transform.position;
		//Vector3 ourPosition = this.transform.position;


		float hipotenusa = Vector3.Distance(magePosition, Enemypos);
		hipotenusa = Mathf.Clamp(hipotenusa, 0.1f, radius);

		float percent = hipotenusa / radius;
		float result = (1.0f - percent) * (max - min) + min;

		result = Mathf.Clamp(result, min, max);

		return result;

	}


	
	// Update is called once per frame
	void Update () {
		
	}
}
