﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserShooter : Base_Weapon {

	float Base_Damage;
	public float Damage;

	//public LaserBullet LB;
	//GameObject LBshot; 
	public AudioClip lasersound;

	public void LaserEm() {
		AudioSource.PlayClipAtPoint(lasersound, this.transform.position);
		 SpawnManager.instance.SpecialSpawn("LaserBullet", transform.position);

	}


	public override void Start() {
		base.Start ();
		Base_Damage = Damage;

	}



	
	// Update is called once per frame
	void Update () {



		#if UNITY_EDITOR
		if (Input.GetMouseButtonDown(0) 
			&& GameManager.instance._currentState == GameManager.GameState.InPlay
			&& MB.Weapon == AttackType.Laser)
			LaserEm();
		#endif
		
	}
}
