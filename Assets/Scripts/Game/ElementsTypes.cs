﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ElementsTypes : MonoBehaviour {

	float[,] MultiplyDamage = new float[(int)AttackType.COUNT, (int)DefenseType.COUNT];

	public Color VeryEfective;
	public Color Efective;
	public Color NotVeryEfective;
	public Color NotEfective;

	public static ElementsTypes instance = null;

	void SetRelation(AttackType Atk, DefenseType Def, float Dmg){	
		MultiplyDamage [(int)Atk, (int)Def] = Dmg;
	}

	public float GetDamage (AttackType Atk, DefenseType Def){
		Debug.Log (Atk + " " + Def);


		return MultiplyDamage [(int)Atk, (int)Def];
	}

	public Color GetColor(float Number){

		if (Number > 1f)
			return VeryEfective;

		if (Number < 1f)
			return NotVeryEfective;

		if (Number == 0f)
			return NotEfective;


			return Efective;
	
	}


	void SetDatabase(){
		for (int i = 0; i < (int)AttackType.COUNT ; i++){
			for (int j = 0; j < (int)DefenseType.COUNT ; j++){
				MultiplyDamage [i,j] = 1f;

			}

		}


	}
	void Awake()
	{
		instance = this;
	}


	void Start(){

		SetDatabase ();
		//Rock
		SetRelation (AttackType.Ice, DefenseType.Rock, 0.5f);
		SetRelation (AttackType.Gravity, DefenseType.Rock, 1.5f);

		//Carbon
		SetRelation (AttackType.Laser, DefenseType.Carbon, 1.5f);
		SetRelation (AttackType.Lightning, DefenseType.Carbon, 1.5f);
		SetRelation (AttackType.Plasma, DefenseType.Carbon, 2f);
		SetRelation (AttackType.Ice, DefenseType.Carbon, 0.5f);
		SetRelation (AttackType.Fire, DefenseType.Carbon, 2f);
		SetRelation (AttackType.Gravity, DefenseType.Carbon, 0.1f);

		//Lava

		SetRelation (AttackType.Plasma, DefenseType.Lava, 0.5f);
		SetRelation (AttackType.Ice, DefenseType.Lava, 2f);
		SetRelation (AttackType.Fire, DefenseType.Lava, 0.5f);

		//Steel
		SetRelation (AttackType.Plasma, DefenseType.Steel, 1.5f);
		SetRelation (AttackType.Fire, DefenseType.Steel, 1.5f);

		//Star
		SetRelation (AttackType.Laser, DefenseType.Star, 0.5f);
		SetRelation (AttackType.Plasma, DefenseType.Star, 0f);
		SetRelation (AttackType.Ice, DefenseType.Star, 2f);
		SetRelation (AttackType.Fire, DefenseType.Star, 0f);


		//Ice

		SetRelation (AttackType.Plasma, DefenseType.Ice, 1.5f);
		SetRelation (AttackType.Fire, DefenseType.Ice, 2f);
		SetRelation (AttackType.Gravity, DefenseType.Ice, 1.5f);

		//Gas
		SetRelation (AttackType.Laser, DefenseType.Ice, 1.5f);
		SetRelation (AttackType.Ice, DefenseType.Ice, 0.5f);
		SetRelation (AttackType.Gravity, DefenseType.Ice, 2f);

		//BlackMatter
		SetRelation (AttackType.Laser, DefenseType.BlackMatter, 0f);
		SetRelation (AttackType.Lightning, DefenseType.BlackMatter, 0.5f);
		SetRelation (AttackType.Plasma, DefenseType.BlackMatter, 0.5f);
		SetRelation (AttackType.Ice, DefenseType.BlackMatter, 0.5f);
		SetRelation (AttackType.Fire, DefenseType.BlackMatter, 0.5f);
		//SetRelation (AttackType.Gravity, DefenseType.BlackMatter, 1f);
	}
}

public enum AttackType
{
    Laser = 0,
    Lightning = 1,
    Fire = 2,
	Plasma = 3,
	Ice = 4,
	Gravity = 5,
	COUNT = 6

}

public enum DefenseType
{
    Rock = 0,
    Carbon = 1,
    Lava = 2,
	Steel = 3,
	Star = 4,
	Ice = 5,
	Gas = 6,
	BlackMatter = 7,
	COUNT = 8
}