﻿using UnityEngine;
using System.Collections;
using System;

public class ComboLevelMessage : IComparable<ComboLevelMessage>
{

    public string Message;
    public int ComboRequired;
    private string v1;
    private int v2;

    public ComboLevelMessage(string v1, int v2)
    {
        Message = v1;
        ComboRequired = v2;
    }


    public int CompareTo(ComboLevelMessage other)
    {
        throw new NotImplementedException();
    }
}
