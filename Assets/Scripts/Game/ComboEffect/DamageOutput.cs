﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DamageOutput : ComboOutput {

    public float damage = 0;
	public Text TextComp;

	public void SetColor(Color Colour){

		Output.color = Colour;
	}

    public override void OnEnable() {

		if (!Animaux) {

			Animaux = this.GetComponentInChildren<Animation> ();
			//TextComp = this.GetComponentsInChildren<Text> ();

		}
        Output.text = (damage*10).ToString("0");
        if (!WhatMessage()) {



			Animaux.Play("DamageOutput");


            Invoke("PullThis", 1f);
        } else {
			Animaux.Play("Message");
            Invoke("PullThis", 1.25f);
        }
        
    }
}
