﻿using UnityEngine;
using System.Collections;

public class SpaceBG : MonoBehaviour {

	float ThresholdA;
	float ThresholdS;
	public float StartingPointAlpha = 820f;
	public float FinishingPointAlpha = 1630f;
	public float StartingPointScale;
	public float FinishingPointScale = 3450f;

	public Material VortexMat = null;
	public float SpeedRot = 10;
	Vector3 StartScale = Vector3.one;
	Vector3 FinalScale = Vector3.one;

	// Use this for initialization
	void Start () {
		this.VortexMat = this.GetComponent<Renderer>().material;
		StartingPointScale = StartingPointAlpha + FinishingPointAlpha;
		StartScale = transform.localScale;
        VortexMat.color = Color.clear;
		FinalScale.x = 45;
		FinalScale.y = 45;
        this.GetComponent<Renderer>().sortingOrder = -2;
	}


	void ResetVertex() {

		transform.localScale = Vector3.Lerp(transform.localScale, StartScale, Time.deltaTime* 2);
		VortexMat.color = Color.Lerp(VortexMat.color,  Color.clear, Time.deltaTime * 2);
	}


	// Update is called once per frame
	void Update () {
		


		if (GameManager.instance.CurrentScore < 5)
			ResetVertex ();

		if (GameManager.instance.CurrentScore > StartingPointAlpha) {
			transform.RotateAround(transform.up, transform.position, Time.deltaTime * SpeedRot);
			ThresholdA = (GameManager.instance.CurrentScore - StartingPointAlpha) / FinishingPointAlpha;
			VortexMat.color = Color.Lerp (Color.clear, Color.white, ThresholdA);
		}
        /*
		if (GameManager.instance.CurrentScore > StartingPointScale) {
			ThresholdS = (GameManager.instance.CurrentScore - StartingPointScale)/FinishingPointScale;
			transform.localScale = Vector3.Lerp (StartScale, FinalScale, ThresholdS);

		}*/
	}
}
