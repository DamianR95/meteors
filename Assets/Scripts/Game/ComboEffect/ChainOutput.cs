﻿using UnityEngine;
using System.Collections;

public class ChainOutput : ComboOutput {

    public override void OnEnable()
    {

		if (!Animaux)
			Animaux = this.GetComponentInChildren<Animation> ();
        //Output.text = (GameManager.instance.CurrentScore * 100).ToString();
        Output.text = (GameManager.instance.MultiplyCombo * 100).ToString();
        if (!WhatMessage())
        {
            SideDecider();
            Invoke("PullThis", 1f);
        }
        else
        {
            this.GetComponentInChildren<Animation>().Play("Message");
            Invoke("PullThis", 1.25f);
        }

        //Output.color = Color.Lerp(GameManager.instance.FColor, GameManager.instance.LColor, GameManager.instance.CurrentScore / GameManager.instance.NextSDLvl);


    }


    public override void Awake()
    {
        Level.Add(new ComboLevelMessage("Yes!", 5));
        Level.Add(new ComboLevelMessage("Level 2!", 10));
        Level.Add(new ComboLevelMessage("Cool!", 30));
        Level.Add(new ComboLevelMessage("Good!", 70));
        Level.Add(new ComboLevelMessage("Level 3!", 100));
        Level.Add(new ComboLevelMessage("Great!", 150));
        Level.Add(new ComboLevelMessage("Sweet!", 400));
        Level.Add(new ComboLevelMessage("Level 4!", 200));
        Level.Add(new ComboLevelMessage("Awesome!", 600));
        Level.Add(new ComboLevelMessage("Level 5!", 800));
        Level.Add(new ComboLevelMessage("Wonderful!", 1500));
        Level.Add(new ComboLevelMessage("Excellent!", 1750));
        Level.Add(new ComboLevelMessage("Stylish!", 2000));
        Level.Add(new ComboLevelMessage("Fantastic!", 2250));
        Level.Add(new ComboLevelMessage("Level 6!", 2000));
        Level.Add(new ComboLevelMessage("Amazing!", 2750));
        Level.Add(new ComboLevelMessage("Incredible!", 3000));
        Level.Add(new ComboLevelMessage("Mighty!", 3500));
        Level.Add(new ComboLevelMessage("Marvelous!", 4000));
        Level.Add(new ComboLevelMessage("Uncanny!", 4750));
        Level.Add(new ComboLevelMessage("Crazy!", 5500));
        Level.Add(new ComboLevelMessage("Galactic!", 6500));
        Level.Add(new ComboLevelMessage("Unstoppable!", 8000));

    }
}
