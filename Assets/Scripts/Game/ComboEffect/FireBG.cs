﻿using UnityEngine;
using System.Collections;

public class FireBG : MonoBehaviour {

	//rock
	//public GameObject Comborockfire;
 
//    Vector3 startingscale = Vector3.zero;
	//Vector3 targetscale = Vector3.zero;
	//bool ImageOn = false;

	//bg
   //Vector3 Localpos = Vector3.zero;
    Vector3 startpos = Vector3.zero;
    Vector3 targetpos = Vector3.zero;
    float index = 2f;
    // Sun
    public GameObject TheSun;
    public GameObject ReferencePoint;
    public Vector3 Distance;
    public float SpeedRot;
    Vector3 StartSunScale = Vector3.one;
    Vector3 FinalSunScale = Vector3.one;
    public Material SunMat = null;
    public Color RedGiant = Color.red;
    float Threshold;
    public float SunAtBig;

    //Earth
    public GameObject Earth;
    public Material EarthMat = null;
    public GameObject Aura;
    float Resistance;

    //Moon
    public GameObject TheMoon;






    void Start () {
		//rock
		//startingscale = Comborockfire.transform.localScale;
		//targetscale = Comborockfire.transform.localScale;
		//Comborockfire.SetActive (false);
		//bg
       // Localpos = transform.position;
       // startpos = transform.position;
        //targetpos = transform.position;
       // targetpos.y = 140;
        //sun
        FinalSunScale.x = 3;
        FinalSunScale.y = 3;
        this.SunMat = TheSun.GetComponent<Renderer>().material;
        this.EarthMat = Earth.GetComponent<Renderer>().material;

    }

    void ResetSun() {

        TheSun.transform.localScale = Vector3.Lerp(TheSun.transform.localScale, StartSunScale, Time.deltaTime* 2);
        SunMat.color = Color.Lerp(SunMat.color, Color.white, Time.deltaTime * 2);
    }

    // Update is called once per frame
    void Update()
    {
        TheSun.transform.RotateAround(ReferencePoint.transform.position, Distance, Time.deltaTime * SpeedRot);
        TheSun.transform.RotateAround(TheSun.transform.up, Vector3.zero, Time.deltaTime * 90f);
        Threshold = GameManager.instance.CurrentScore / SunAtBig;

        TheMoon.transform.RotateAround(ReferencePoint.transform.position, Distance, Time.deltaTime * SpeedRot);

        Resistance = GameManager.instance.LivesCount;
        EarthMat.color = Color.Lerp(RedGiant, Color.white, Resistance/3f);


        if (GameManager.instance.CurrentScore < 5)
			ResetSun ();

        if ( GameManager.instance.CurrentScore == 0)
		{
			//targetscale.y = targetscale.x;
			//Comborockfire.transform.localScale = targetscale;
			//Comborockfire.SetActive (false);
			//ImageOn = false;


			transform.position = Vector3.Lerp(startpos, targetpos, index);


		}


        if(GameManager.instance.MultiplyCombo >= 10)
            Aura.GetComponent<Animation>().Play("FlashingColors");
        else
            Aura.GetComponent<Animation>().Stop("FlashingColors");

        if (GameManager.instance.CurrentScore > 25)
        {
            TheSun.transform.localScale = Vector3.Lerp(StartSunScale, FinalSunScale, Threshold);
            SunMat.color = Color.Lerp(Color.white, RedGiant, Threshold);
        }



			
		/*if (GameManager.instance.CurrentCombo >= 15) {
			if (!ImageOn) {
				Comborockfire.SetActive (true);
				ImageOn = true;
			}         
		}*/
			
		/*if ( GameManager.instance.CurrentCombo >= 32)
		{
			targetscale.y = GameManager.instance.CurrentCombo;
			Comborockfire.transform.localScale = targetscale;
		}*/
		



    }
}
