﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ComboEnded : MonoBehaviour {

    public Text LastChain;
    public Text CoinGained;
    protected int LastCombo;
    protected int CoinsToGain;

    public void PlayThis(int TheCombo)
    {
        LastCombo = TheCombo;
        this.GetComponent<Animation>().PlayQueued("FinishedCombo");

    }

    public void ChainMessage()
    {
        LastChain.text = LastCombo.ToString() + " Chain!";

    }

    public void CoinMessage()
    {
        CoinsToGain = LastCombo / 10;
        /*CoinGained.text = CoinsToGain.ToString() + " Bonus Coins!!";
        if (CoinsToGain == 1)
        CoinGained.text = CoinsToGain.ToString() + " Bonus Coin!";*/

        CoinGained.text = CoinsToGain != 1 ?
            CoinGained.text = CoinsToGain.ToString() + " Bonus Coins!!" :
            CoinGained.text = CoinsToGain.ToString() + " Bonus Coin!";
    }

    public void WinningCoins()
    {
        GameManager.instance.CurrentCoins += CoinsToGain;
        GameManager.instance.CoinText.GetComponent<Animation>().Play("CoinTextGlow");

    }

}
