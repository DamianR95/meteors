﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ComboOutput : MonoBehaviour {

    //public string[] Cheers;
    //public int[] ComboLevel;

  
    public List<ComboLevelMessage> Level = new List<ComboLevelMessage>();
	protected Animation Animaux;


    public Text Output = null;

   public virtual void OnEnable() {

		if (!Animaux)
			Animaux = this.GetComponentInChildren<Animation> ();

        Output.text = GameManager.instance.CurrentCombo.ToString()+"!";

        if (!WhatMessage())
        {
            //SideDecider();
			Animaux.Play("ComboFall");

           Invoke("PullThis", 1f);
        }
        else { 
			Animaux.Play("Message");
            Invoke("PullThis", 1.25f);
        }

       // Output.color = Color.Lerp(GameManager.instance.FColor, GameManager.instance.LColor, GameManager.instance.CurrentScore / GameManager.instance.NextSDLvl);
		

		}

    public bool WhatMessage() {

        foreach (ComboLevelMessage Lvl in Level) {
            if (GameManager.instance.CurrentScore == Lvl.ComboRequired)
            {
                //Debug.Log(Lvl.Message);
                Output.text = Lvl.Message;
                return true;
            }
        }
        return false;
    }

    public void SideDecider() {
        int LeftRight = Random.Range(0, 2);
        if (LeftRight == 1)
			Animaux.Play("ComboOutput");
        else
			Animaux.Play("ReverseComboOutput");
    }


	public void PullThis(){
        //Output.color = Color.white;
		ObjectPool.instance.PoolObject (this.gameObject);
	
	
	}
	// Update is called once per frame
	public  virtual void Awake () {
      
     
    }
}

