﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MeteorBehaviour : MonoBehaviour {

    protected Rigidbody2D rb = null;
    public int Attack = 1;
    public float Mlife = 1;
    public float Life = 1;
    public int Drops;
    public float maxspeed;
    public float minspeed;
    public float currentspeed;
    float multiplyspeed;
    protected float step;

    public DefenseType EnemyType;
    // protected float BonusDamage = 1;

    public static MeteorBehaviour instance = null;
    public AudioClip meteorsound;
    public AudioClip FallingMeteor;


    protected SpriteRenderer sr;
    public Sprite[] ObjectSprite;
    public GameObject PrefabLifeBar;
    public Slider Lifebar;
    public Vector3 LifeBarScale;
    float MaxLife;
    public Vector3 Offset;

    public Vector2 whereToLook;

    public float InvulnerableTime = 0.25f;
    private float TimeIf_CanBehurt = 0.25f;
    private float TimeSinceSpawn = 0f;
    private float TimetoSpeedUp = 2f;
    // private float TimertoGo = 4f;
    public bool CanBeHurtByPowerUP = true;
    public Material MeteorMat = null;
    private Color[] _lerpingColors = { Color.red, Color.blue };

    public GameObject mage;
    protected MageBehaviour MB;
	protected LightningShooter LS;
    public bool IsAirCraft = false;
    public bool IsGoingToTheMage = true;

    // bool Sign = false;
    // public GameObject Exclamation = null;



    public Vector2 auxVector;

    void Awake() {
        instance = this;
    }


    public void ApplyDamager(int d) {
        //AttackType AtkType = MB.Weapon;
        lightning_start.instance.ZapTarget(this.gameObject);
		float distancedamage = LS.CalculateDistance(this.transform.position);
		Damager(distancedamage * LS.Damage, MB.Weapon);
    }

    // Use this for initialization
    protected virtual void Start() {
        rb = this.gameObject.GetComponent<Rigidbody2D>();
        this.MeteorMat = this.GetComponent<Renderer>().material;

    }

    protected virtual void ChangeSprite() {
        int Randomness = Random.Range(0, ObjectSprite.Length);

        sr.sprite = ObjectSprite[Randomness];

    }


    protected virtual void OnEnable() {
        if (!mage) {
            mage = GameManager.instance.PlayerRef.gameObject;
            MB = mage.GetComponent<MageBehaviour>();
			LS = mage.GetComponent<LightningShooter> ();
        }


        currentspeed = Random.Range(minspeed, maxspeed);
        multiplyspeed = Mathf.Lerp(1f, 1.5f, GameManager.instance.CurrentScore / 10000f);

        TimeSinceSpawn = 0f;
        currentspeed = currentspeed * multiplyspeed;
        if (rb == null)
            rb = this.GetComponent<Rigidbody2D>();
        if (sr == null)
            sr = GetComponent<SpriteRenderer>();
        // Sign = false;
        //BonusDamage = 1;

        if (ObjectSprite.Length != 0)
            ChangeSprite();

        SettingLife();


        Vector2[] cameraBounds = GameManager.instance.GetVectorFromCameraBound();

        whereToLook = new Vector2(Random.Range(cameraBounds[0].x, cameraBounds[1].x), cameraBounds[0].y);

        auxVector = whereToLook;
    }

    public void SettingLife() {
        float desiredMultiply = GameManager.instance.MultiplyCombo / 3.4f;

        float multiplier = desiredMultiply < 1 ? 1 : desiredMultiply;
        multiplier = multiplier <= 1 ? 1 : multiplier / 2f;
        multiplier = Mathf.Clamp(multiplier, 1, 2);

        Life = Mlife * multiplier;
        MaxLife = Life;
    }


    void OnDrawGizmosSelected() {
        Debug.DrawLine(transform.position, auxVector);
    }

    public int Randomness(int maxrange) {
        int Number = Random.Range(0, maxrange);
        return Number;
    }



    public virtual void Damager(float Damage, AttackType AtkType) {
        //Debug.Log (EnemyType);
        float auxDamage = Damage;
        float MultiplyDamage = ElementsTypes.instance.GetDamage(AtkType, EnemyType);

        auxDamage *= MultiplyDamage;
        //  Damage *= BonusDamage;
        SpawnManager.instance.DamageSpawn("Output_Damage", transform.position + Offset, auxDamage,
                                            ElementsTypes.instance.GetColor(MultiplyDamage));
        Life = Life - auxDamage;

        GameManager.instance.TimerSlider.value += (0.01f * auxDamage);

        if (auxDamage > 0) {
            AudioSource.PlayClipAtPoint(meteorsound, this.transform.position);

        }

        if (auxDamage >= 1)
            CameraControl.instance.Shake(0.2f, 3, 5f);

        //CameraControl.instance.Move(0f, 10f, 15f);

        if (Life > 0) {
            StartCoroutine(Blink());
            GameManager.instance.NewChainRecord();

            if (Lifebar == null) {
                SpawnManager.instance.LifebarSpawn("LifeBar", transform.position, this.gameObject);
                PrefabLifeBar.transform.localScale = LifeBarScale;
                Lifebar = PrefabLifeBar.GetComponent<Slider>();
            }
            Lifebar.value = Mathf.Lerp(0, 1, Life / MaxLife);


        }
        if (Life <= 0) {

            StopCoroutine(Blink());
            MeteorMat.color = Color.white;

            KillThis();
            //Continue 2
            if (Tutorial_Behaviour.isTutorial())
                Tutorial_Behaviour.instance.Continue("Destroyed");
            //
            GameManager.instance.RisingMeteorCount();
            SpawnManager.instance.SpecialSpawn("ExplosionMobile", transform.position);


            CoinDrop(Drops);

        }

    }

    public virtual void KillThis() {

        if (PrefabLifeBar) {
            ObjectPool.instance.PoolObject(PrefabLifeBar);
            PrefabLifeBar = null;
            Lifebar = null;
        }
        if (GameManager.instance.GameStarted == false)
            GameManager.instance.GameStarted = true;

        TransparentScript.instance.RemoveThis(this.gameObject);
        ObjectPool.instance.PoolObject(this.gameObject);
    }


    public void CoinDrop(int Dropeable) {
        if (GameManager.instance.MultiplyCombo >= 10)
            Dropeable *= 2;

        if (GameManager.instance.MultiplyCombo >= 15)
            Dropeable *= 2;


        for (int i = 0; i < Dropeable; i++) {

            if (Randomness(2) == 0)
                SpawnManager.instance.SpecialSpawn("Coin", transform.position);
            if (Randomness(4) == 0)
                SpawnManager.instance.SpecialSpawn("Fuel", transform.position);
        }


    }



#if UNITY_EDITOR

    void OnMouseDown() {
        if (GameManager.instance._currentState == GameManager.GameState.InPlay 
			&& MB.Weapon == AttackType.Lightning) {

			ApplyDamager (1);
        }

    }
#endif

  

    // Update is called once per frame
    protected virtual void Update() {
        CheckIfNeedToDie();

        if (Lifebar != null) {

            /* Offset = transform.position.y + 1f;
             AuxVec = transform.position;
             AuxVec.y = transform.position.y + Offset;*/
            Lifebar.transform.position = transform.position + Offset;


        }


        TimeIf_CanBehurt -= Time.deltaTime;

        if (TimeIf_CanBehurt <= 0) {
            CanBeHurtByPowerUP = true;
        }
        TimeSinceSpawn += Time.deltaTime;

        if (rb && !IsAirCraft) {
            Vector2 pos = transform.position;

            var dir = (auxVector - pos).normalized * currentspeed;

            rb.velocity = Vector2.Lerp(Vector2.zero, dir, TimeSinceSpawn / TimetoSpeedUp);
            // rb.velocity = dir;
            //Debug.Log(Time.deltaTime*10);
        }
    }

    public IEnumerator Blink() {
        float time = InvulnerableTime;
        float elapsed = 0f;
        int index = 0;
        currentspeed = currentspeed / 2;
        float interval = 0.1f;
        while (elapsed < time) {
            MeteorMat.color = _lerpingColors[index % 2];
            index++;
            elapsed += Time.deltaTime + interval;
            yield return new WaitForSeconds(interval);
        }

        MeteorMat.color = Color.white;
        currentspeed = currentspeed * 2;
    }




    public virtual void OnTriggerStay2D(Collider2D other) {
        if (CanBeHurtByPowerUP) {
            if (other.tag == "GravityBullet" && CanBeHurtByPowerUP) {

                TimeIf_CanBehurt = InvulnerableTime;
                CanBeHurtByPowerUP = false;

                GravityBullet GB = other.gameObject.GetComponent<GravityBullet>();
                //auxVector = other.transform.position;
                Damager(GB.Damage, GB.AtkType);

            }

            if (other.tag == "Fire") {

                TimeIf_CanBehurt = InvulnerableTime;
                CanBeHurtByPowerUP = false;

                Damager(5, AttackType.Fire);
                GameManager.instance.TimerSlider.value += 0.05f;
            }
            if (other.tag == "Lightning") {

                TimeIf_CanBehurt = InvulnerableTime;
                CanBeHurtByPowerUP = false;

                Damager(1, AttackType.Lightning);
            }
        }
    }



    public virtual void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Bullet") {
            Bullet Bt = other.gameObject.GetComponent<Bullet>();
			Debug.Log (Bt.Damage);
            float auxlife = Life;
            Damager(Bt.Damage, Bt.AtkType);
            Bt.Damage -= auxlife;

            if (Bt.Damage <= 0f)
                Bt.SelfDestruct();
        }



        if (other.tag == "Ice") {
            //rb.velocity = (Vector3.zero);
            //rb.gravityScale = rb.gravityScale * 0.25f;
            currentspeed /= 10;
            GameManager.instance.TimerSlider.value += 0.03f;
        }

        if (other.tag == "Mage") {
            KillThis();
        }

    }

    public virtual void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Ice") {
            currentspeed *= 5;
        }

        if (other.tag == "GravityBullet") {
            // auxVector = whereToLook;


        }


    }

    public virtual void CheckIfNeedToDie() {
        Vector3 a = Camera.main.WorldToViewportPoint(transform.position);
        if (a.x <= -0.05 || a.x >= 1.05) {
            KillThis();
        }

        if (a.y <= 0.5) {

            MeteorMat.color = Color.Lerp(Color.red, Color.white, a.y / 0.5f);
            //AlertSound
            //AudioSource.PlayClipAtPoint(FallingMeteor, this.transform.position);

        }
        if (a.y <= 0.1) {
            if (a.x >= 0 && a.x <= 1) {
                SpawnManager.instance.SpecialSpawn("EarthDamage", transform.position);
                AudioSource.PlayClipAtPoint(FallingMeteor, this.transform.position);
                CameraControl.instance.Shake(0.2f, 7, 3f);
                GameManager.instance.LosingLife(Attack);
            }
            KillThis();

        }
    }


}
