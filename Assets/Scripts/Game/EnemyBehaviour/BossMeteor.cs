﻿using UnityEngine;
using System.Collections;

public class BossMeteor : MeteorBehaviour
{

	public DefenseType[] possibleTypes;

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    /*public override void  OnTriggerEnter2D(Collider2D other) {
		//Destroy(other.gameObject);
		if (other.tag == "Fire") {
			Damager (5);
		}
		if (other.tag == "Ice") {
			rb.velocity = (Vector3.zero);
			rb.gravityScale = rb.gravityScale * 0.25f;
		}
	}*/

	protected override void ChangeSprite()
	{
		int Randomness = Random.Range(0, ObjectSprite.Length);

		sr.sprite = ObjectSprite[Randomness];

		EnemyType = possibleTypes [Randomness];

	}


    public override void KillThis()
    {
        if (PrefabLifeBar)
        {
            ObjectPool.instance.PoolObject(PrefabLifeBar);
            PrefabLifeBar = null;
            Lifebar = null;
        }
        TransparentScript.instance.RemoveThis(this.gameObject);
        GameManager.instance.toggleBGM();
        ObjectPool.instance.PoolObject(this.gameObject);
    }

    public override void CheckIfNeedToDie()
    {
        Vector3 a = Camera.main.WorldToViewportPoint(transform.position);
        if (a.x <= -0.2 || a.x >= 1.2)
        {
            KillThis();
           

        }
        if (a.y <= 0.5)
        {

            MeteorMat.color = Color.Lerp(Color.red, Color.white, a.y / 0.5f);
            //AlertSound
            //AudioSource.PlayClipAtPoint(FallingMeteor, this.transform.position);

        }
        if (a.y <= -0)
        {
            ObjectPool.instance.PoolObject(this.gameObject);
            // LoseLive.
            KillThis();
            GameManager.instance.LosingLife(Attack);
        }
    }

    /*public override void Damager(float Damage)
	{
		base.Damager (Damage);
		if (Life <= 0) {
			GameManager.instance.toggleBGM();
		}
	}*/
}
