﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class EnemyShield : MonoBehaviour {

    public List<GameObject> currentShielded = new List<GameObject>();

    bool ShieldOnEnabled = false;



    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        ShieldOn(other);

    }


    public virtual void OnTriggerStay2D(Collider2D other)
    {
        if (ShieldOnEnabled) return;
        ShieldOn(other);

    }
    void ShieldOn(Collider2D other){

        if (other.tag == "Meteor")
        {
            currentShielded.Add(other.gameObject);
            other.gameObject.layer = 2;
            ShieldOnEnabled = true;
        }
    }


    void ShieldOff(Collider2D other)
    {

        if (other.tag == "Meteor")
        {
            currentShielded.Remove(other.gameObject);
            other.gameObject.layer = 8;
        }
    }






    public virtual void OnTriggerExit2D(Collider2D other)
    {

        ShieldOff(other);

    }

        void OnEnable()
    {

       

    }

    void OnDisable() {
      

        // Print the entire list to the console.
        foreach (GameObject gObject in currentShielded)
        {
            
            gObject.gameObject.layer = 8;
            //currentShielded.Remove(gObject.gameObject);
        }
        ShieldOnEnabled = false;
        currentShielded.Clear();
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
