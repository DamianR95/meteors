﻿using UnityEngine;
using System.Collections;

public class TheMoon : MonoBehaviour
{
    int Endurance = 50;

    public void ApplyDamager(int d)
    {

        lightning_start.instance.ZapTarget(this.gameObject);
        Damager(d);
        //Debug.Log("Apply Damager");
    }
#if UNITY_EDITOR

    void OnMouseDown()
    {
        if (GameManager.instance._currentState == GameManager.GameState.InPlay)
        {

            lightning_start.instance.ZapTarget(this.gameObject);

            Damager(1);


        }

    }
#endif
    void Damager(int damage)
    {
       
        if(GameManager.instance.StardustLevel>7)
        Endurance -= damage;

        if(Endurance == 0)
        {
            ObjectPool.instance.PoolObject(this.gameObject);
			SpawnManager.instance.SpecialSpawn("ExplosionMobile", transform.position);
            GameManager.instance.RisingMeteorCount();
        }

    }



}
