﻿using UnityEngine;
using System.Collections;

public class AircraftBehaviour : MeteorBehaviour{

    public GameObject m_target = null;
    float DistancefromTargetX;

    //Vector3 scale = Vector3.zero;
    GameObject go;
 
 

  

    private Vector3 targetAngles;


    protected override void Update()
    {
        base.Update();


        if (IsGoingToTheMage)
        {
            GoingAtTarget(mage);
        }
        else
        {
            GoingAtTarget(m_target);
         }
    }

    void GoingAtTarget(GameObject target)
    {
		step = currentspeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, step);

            DistancefromTargetX = transform.position.x - target.transform.position.x;

        if (DistancefromTargetX < 0)
        {
            transform.rotation = Quaternion.identity;
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }


    protected override void OnEnable()
    {
        //base.OnEnable();
        //if (!IsGoingToTheMage) IsGoingToTheMage = true;
        //else IsGoingToTheMage = false;
        /*if (Lifebar != null)
        {
            Lifebar.value = 1;
            Lifebar.gameObject.SetActive(false);
        }*/
		if (!mage) { 
			mage = GameManager.instance.PlayerRef.gameObject;
			MB = mage.GetComponent<MageBehaviour>();
		}

		currentspeed = 1f;

		if (rb == null)
			rb = this.GetComponent<Rigidbody2D>();

		//Life = Mlife;
        SettingLife();


        go = this.transform.Find("MotorSound").gameObject;
        go.GetComponent<AudioSource>().Play();

    }

 


    public override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Ice")
        {
			currentspeed = 0.5f;
        }
		if (other.tag == "Bullet") {
			Bullet Bt = other.gameObject.GetComponent<Bullet> ();
			float auxlife = Life;
			Damager(Bt.Damage, Bt.AtkType);
			Bt.Damage -= auxlife;

			if (Bt.Damage <= 0f)
				Bt.SelfDestruct ();

		}

        if (other.tag == "Mage") {
            KillThis();
        }
    }

    public override void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Ice")
        {
			currentspeed = 1f;
        }
        
    }



    public override void CheckIfNeedToDie()
    {
        Vector3 a = Camera.main.WorldToViewportPoint(transform.position);
        if (a.x <= -0.3 || a.x >= 1.3)
        {
			KillThis();
        }
        if (a.y <= 0)
        {
			KillThis ();
			// LoseLive.
            GameManager.instance.LosingLife(Attack);
        }
    }



}
