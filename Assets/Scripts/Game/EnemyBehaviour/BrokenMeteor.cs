﻿using UnityEngine;
using System.Collections;

public class BrokenMeteor : MeteorBehaviour {

    Vector3 Localpos = Vector3.zero;
    Vector3 Spawnpos1 = Vector3.zero;
    Vector3 Spawnpos2 = Vector3.zero;
    Vector3 Spawnpos3 = Vector3.zero;
    Vector3 Spawnpos4 = Vector3.zero;


 


    protected override void OnEnable()
    {
        base.OnEnable();
    }

	public override void Damager(float Damage, AttackType AtkType)
    {
		base.Damager(Damage, AtkType);
        if (Life <= 0)
        {
            Localpos = transform.position;

            Spawnpos1.x = Localpos.x - 1f;
            Spawnpos1.y = Localpos.y;
            Spawnpos1.z = Localpos.z;

            Spawnpos2.x = Localpos.x + 1f;
            Spawnpos2.y = Localpos.y;
            Spawnpos2.z = Localpos.z;

            Spawnpos3.x = Localpos.x;
            Spawnpos3.y = Localpos.y - 1f;
            Spawnpos3.z = Localpos.z;

            Spawnpos4.x = Localpos.x;
            Spawnpos4.y = Localpos.y + 1f;
            Spawnpos4.z = Localpos.z;
  
			SpawnManager.instance.SpecialSpawn("Meteor", Spawnpos1);

            SpawnManager.instance.SpecialSpawn("Meteor", Spawnpos2); 



            if (GameManager.instance.StardustLevel >= 4)
				SpawnManager.instance.SpecialSpawn("Meteor", Spawnpos3);

            if (GameManager.instance.StardustLevel >= 5)
				SpawnManager.instance.SpecialSpawn("Meteor", Spawnpos4);

        }
    }

}
