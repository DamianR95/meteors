﻿using UnityEngine;
using System.Collections;

public class S_AircraftBehaviour : AircraftBehaviour {


	public GameObject Shield;



    
        void OnShield(){
            Shield.SetActive (true);
           // this.gameObject.layer = 2;
			EnemyType = DefenseType.BlackMatter;
            Invoke("OffShield",1f);
        }

        void OffShield(){
            Shield.SetActive (false);
            //this.gameObject.layer = 8;
			EnemyType = DefenseType.Steel;
            Invoke("OnShield",3f);
        }
        
    protected override void OnEnable()
	{
       
        CancelInvoke("OffShield");
        CancelInvoke("OnShield");
        base.OnEnable();
		OnShield ();
	}

	
}
