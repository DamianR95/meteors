﻿using UnityEngine;
using System.Collections;

public class PowerStorer : MonoBehaviour {

	public float TimeForPool = 4f;
	private float TimertoGo = 4f;
	public Material mat = null;
	private Color[] _lerpingColors = { Color.white, Color.clear };
	bool canblink = true;
    GameObject mage;

    public bool HasSound = false;
    //AudioSource go;
    GameObject go;

    //	void OnEnable(){
    //		TimertoGo -= Time.deltaTime;
    //		if(TimertoGo <= 0){
    //			ObjectPool.instance.PoolObject (this.gameObject);
    //			TimertoGo = 4.0f;
    //		}
    //	}
    void Start(){
	   TimertoGo = TimeForPool;
		this.mat = this.GetComponent<Renderer> ().material;
       


    }
    void OnEnable()
    {
        if (HasSound)
        {
            go = this.transform.Find("LightningSound").gameObject;
            go.GetComponent<AudioSource>().Play();
        }
        // if(HasSound)    AudioSource.PlayClipAtPoint(Sound, this.transform.position);
        mage = GameManager.instance.PlayerRef.gameObject;
    }

	void Update(){

        


            TimertoGo -= Time.deltaTime;
		if (TimertoGo <= 2f && canblink) {
		StartCoroutine(Blink ());
			canblink = false;
		}
		if(TimertoGo <= 0){
			ObjectPool.instance.PoolObject (this.gameObject);
            mage.SendMessage("PlayAnimation", "Ship");
			TimertoGo = TimeForPool;
            mat.color = Color.white;
			canblink = true;
		}
	}

	public IEnumerator  Blink(){
		float time = 2f;
		float elapsed = 0f;
		int index = 0;

		float interval = 0.1f;
		while (elapsed < time) {
			mat.color = _lerpingColors[index % 2];
			index++;
			elapsed += Time.deltaTime + interval;
			yield return new WaitForSeconds(interval);
		}

		mat.color = Color.white;
	}
}
