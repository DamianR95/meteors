﻿using UnityEngine;
using System.Collections;

public class Pooler : MonoBehaviour
{

    public float TimeForPool = 2f;
    private float TimertoGo = 2f;
  

    //	void OnEnable(){
    //		TimertoGo -= Time.deltaTime;
    //		if(TimertoGo <= 0){
    //			ObjectPool.instance.PoolObject (this.gameObject);
    //			TimertoGo = 4.0f;
    //		}
    //	}
    void Start()
    {
        TimertoGo = TimeForPool;
 

    }

    void Update()
    {
        TimertoGo -= Time.deltaTime;
       
        if (TimertoGo <= 0)
        {
            ObjectPool.instance.PoolObject(this.gameObject);
            TimertoGo = TimeForPool;

        }
    }

    
}
