﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelPerk : MonoBehaviour {

    public GameObject[] WeaponButton;
    public bool PerkOn = false;
    public static PanelPerk instance = null;


    public Animation ThisWindow;
    MageBehaviour MB;
    PlasmaShooter PS;
    GravityShooter GS;

    public void PerkWindowToggle() {

        

        if(GameManager.instance._currentState != GameManager.GameState.InPerks)
             GameManager.instance.ChangeState(GameManager.GameState.InPerks);
        else
            GameManager.instance.ChangeState(GameManager.GameState.InPlay);
    }


    public void AnimationWindow()
    {
		if (PS.ChargingPlasma)
			return;

        if (GS.ChargingGravity)
            return;

        if (PerkOn)
        {
            PerkOn = false;
            ThisWindow.PlayQueued("PerkWindowOut");
        }
        else
            {
            PerkOn = true;

         
            //else
                ThisWindow.PlayQueued("PerkWindow");

             }
    }

    public void SetLaser()
    {
        MB.Weapon = AttackType.Laser;
		AnimationWindow();

    }
    public void SetLightning()
    {
        MB.Weapon = AttackType.Lightning;
		AnimationWindow();

    }

	public void SetPlasma()
	{
		MB.Weapon = AttackType.Plasma;
		AnimationWindow();

	}

    public void SetGravity()
    {
        MB.Weapon = AttackType.Gravity;
        AnimationWindow();

    }

    void Awake()
    {
        instance = this;
       
    }


    void OnEnable() {

    }
    // Use this for initialization
    void Start () {
        ThisWindow = this.GetComponent<Animation>();


    }

    // Update is called once per frame
    void Update () {

        if (!MB)
        {
            MB = GameManager.instance.PlayerRef.gameObject.GetComponent<MageBehaviour>();
            PS = GameManager.instance.PlayerRef.gameObject.GetComponent<PlasmaShooter>();
            GS = GameManager.instance.PlayerRef.gameObject.GetComponent<GravityShooter>();
        }




    }
}
