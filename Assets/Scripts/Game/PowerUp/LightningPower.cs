﻿using UnityEngine;
using System.Collections;

public class LightningPower : PowerUps
{

    Vector3 LightningLocal = Vector3.zero;

    


    public override void ExecutePowerUP()
    {
       // AudioSource.PlayClipAtPoint(Upsound, this.transform.position);
		SpawnManager.instance.MessageSpawn("Output_Message", transform.position, Message);
        go = ObjectPool.instance.GetObjectForType("Lightning");
        mage.SendMessage("PlayAnimation", "ShipLightningUp");
        //base.ExecutePowerUP();
        LightningLocal.x = mage.transform.position.x + 0.2f;
        LightningLocal.y = mage.transform.position.y + 6f;
  
        go.transform.position = LightningLocal;
		go.transform.parent = mage.transform;
    }



}
