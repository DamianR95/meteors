﻿using UnityEngine;
using System.Collections;

public class PowerUps : MeteorBehaviour {


	//public GameObject mage = null;
	public GameObject go = null;
	public AudioClip Upsound;
	public GameObject staff;
    public string Message;
    

	void OnMouseDown(){

	}

   
	public override void  OnTriggerEnter2D(Collider2D other) {
       // mage = GameObject.FindWithTag("Mage");
		//staff =  GameObject.FindWithTag("Staff");
        if (other.tag == "Mage") {;
			GameManager.instance.NewChainRecord();
			if (GameManager.instance.GameStarted == false)
				GameManager.instance.GameStarted = true;
			//GameManager.instance.GuiSpawn ("Output", transform.position);
			ExecutePowerUP();
		}
		
	}
    public override void OnTriggerStay2D(Collider2D other)
    {
         }

    public override void OnTriggerExit2D(Collider2D other) {
    }

    public virtual void ExecutePowerUP()
	{
		SpawnManager.instance.MessageSpawn("Output_Message",transform.position,Message);
		AudioSource.PlayClipAtPoint (Upsound, this.transform.position);
		go.transform.position = mage.transform.position;
		go.transform.parent = mage.transform;
	}

	/* protected override void Update(){
		CheckIfNeedToDie ();
		step = currentspeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, mage.transform.position, step);

    }*/

	//protected override void OnEnable(){
		
		//mage = GameObject.FindWithTag("Mage");
//	}

	
	public override void CheckIfNeedToDie(){

		Vector3 a = Camera.main.WorldToViewportPoint (transform.position);
		if (a.x <= -0.05 || a.x >= 1.05 || a.y <= 0) {
			if(Tutorial_Behaviour.isTutorial())
				SpawnManager.instance.Spawn("PowerUp");
			if (GameManager.instance.GameStarted == false)
				SpawnManager.instance.Spawn ("LightningUpTuto");
			ObjectPool.instance.PoolObject (this.gameObject);
		}
	}
}
