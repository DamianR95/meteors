﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {


	public static SpawnManager instance = null;


	[SerializeField]
	public GameObject goInScene = null;
	[SerializeField]
	private GameObject guiInScene = null;

	//SPAWNER
	Vector3 Spawnpos = Vector3.zero;

	public void Spawn(string namespawn) {

		if (namespawn == "PowerUp") {
			//namespawn = PowerDecide();
		}

		GameObject go = ObjectPool.instance.GetObjectForType(namespawn);


		Spawnpos.x = Random.Range(0.1f, 0.9f);
		Spawnpos.y = 1.3f;
		Spawnpos.z = 0;

		Vector3 g = Camera.main.ViewportToWorldPoint(Spawnpos);
		g.z = 0;

		//Debug.Log (g);
		go.transform.position = g;
		go.transform.parent = goInScene.transform;
	}

	public void SideSpawn(string namespawn) {



		if (namespawn == "PowerUp") {
			//namespawn = PowerDecide();

		}

		GameObject go = ObjectPool.instance.GetObjectForType(namespawn);


		int LeftorRight = Random.Range(0, 2);

		if (LeftorRight == 1)
			Spawnpos.x = -0.2f;
		else
			Spawnpos.x = 1.2f;

		Spawnpos.y = Random.Range(0.5f, 1.3f);
		Spawnpos.z = 0;

		Vector3 g = Camera.main.ViewportToWorldPoint(Spawnpos);
		g.z = 0;

		//Debug.Log (g);
		go.transform.position = g;
		go.transform.parent = goInScene.transform;
	}

	public void SpecialSpawn(string namespawn, Vector3 Position) {

		if (namespawn == "PowerUp") {
			//namespawn = PowerDecide();

		}

		GameObject go = ObjectPool.instance.GetObjectForType(namespawn);
		go.transform.position = Position;
		go.transform.parent = goInScene.transform;
	}

	public void DamageSpawn(string namespawn, Vector3 Position, float damage, Color DamageType){


		GameObject go = ObjectPool.instance.GetObjectForType(namespawn, false, false);

		DamageOutput DO = go.GetComponent<DamageOutput> ();
		DO.damage = damage;
		DO.SetColor (DamageType);

		go.SetActive(true);
		go.transform.position = Position;
		go.transform.SetParent(guiInScene.transform);
		//go.transform.parent = goInScene.transform;
		go.transform.localScale = Vector3.one;
	}

	public void LifebarSpawn(string namespawn, Vector3 Position, GameObject enemy){
	

		GameObject go = ObjectPool.instance.GetObjectForType(namespawn, false, false);

		enemy.GetComponent<MeteorBehaviour>().PrefabLifeBar = go;


		go.SetActive(true);
		go.transform.position = Position;
		go.transform.SetParent(guiInScene.transform);
		//go.transform.parent = goInScene.transform;
		go.transform.localScale = Vector3.one;
	
	}

	public void MessageSpawn(string namespawn, Vector3 Position, string Message = null){
	

		GameObject go = ObjectPool.instance.GetObjectForType(namespawn, false, false);

		if(Message != null)
		go.GetComponent<AnimationPooler>().Message = Message;

		go.SetActive(true);
		go.transform.position = Position;
		go.transform.SetParent(guiInScene.transform);
		//go.transform.parent = goInScene.transform;
		go.transform.localScale = Vector3.one;
	
	}

	/*public void GuiSpawn(string namespawn, Vector3 Position, float damage = 0, string Message = null, GameObject enemy = null) {


		GameObject go = ObjectPool.instance.GetObjectForType(namespawn, false, false);

		if (damage != 0) {
			//Debug.Log (Position);
			go.GetComponent<DamageOutput>().damage = damage;
		}
		if (Message != null)
		{
			go.GetComponent<AnimationPooler>().Message = Message;
		}
		if(enemy!= null)
		{

			enemy.GetComponent<MeteorBehaviour>().PrefabLifeBar = go;
		}
		go.SetActive(true);
		go.transform.position = Position;
		go.transform.SetParent(guiInScene.transform);
		//go.transform.parent = goInScene.transform;
		go.transform.localScale = Vector3.one;

	}*/


	void Awake()
	{
		instance = this;
	}


}
