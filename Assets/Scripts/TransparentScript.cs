﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class TransparentScript : MonoBehaviour {

	public List<GameObject> OverlapOnGui = new List<GameObject>();
	bool isTransparent = false;
	public Material Trans;
    public static TransparentScript instance = null;

    //Color HalfVisible = new Color (255, 255, 255, 125);
    //Color Normal = new Color (255, 255, 255, 255);

    public Text title;
	public Text Number;
	public Text Multiply;
    public Image BG;


   // Text titletext;
   // Text numbertext;
    //Text multiplytext;
    
	/*public Material TitleMat;
	public Material NumberMat;
	public Material MultiplyMat;*/


	void OnTriggerEnter2D(Collider2D other){

        if(other.gameObject.name !="Shield")
		OverlapOnGui.Add(other.gameObject);

	}

	void OnTriggerExit2D(Collider2D other){

		OverlapOnGui.Remove(other.gameObject);

	}

    public void RemoveThis(GameObject other) {
        if(OverlapOnGui.Contains(other))
            OverlapOnGui.Remove(other.gameObject);
    }
	void Transparence(){
       /* TitleMat = Trans;
		NumberMat = Trans;
		MultiplyMat = Trans;*/
        title.material = Trans;
       Number.material = Trans;
        Multiply.material = Trans;
        BG.material = Trans;
        // Debug.Log ("trans");
    }

	void Visible(){
        /*TitleMat = null;
		NumberMat =  null;
		MultiplyMat =  null;*/
        title.material = null;
        Number.material = null;
        Multiply.material = null;
        BG.material = null;
    }

    // Use this for initialization
    void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update () {
	
		if (OverlapOnGui.Count > 0 && !isTransparent) {
			isTransparent = true;
			Transparence ();
		}
	
		if(OverlapOnGui.Count == 0)
		{
			isTransparent = false;
			Visible ();
				}
}
}