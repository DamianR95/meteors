﻿using UnityEngine;
using System.Collections;

public class PoolGameObject : MonoBehaviour 
{
	ObjectPool pool;
	
	public void SetPool(ObjectPool pool)
	{
		this.pool = pool;
	}
	
	public void Recycle(){
		if (pool != null)
			ObjectPool.instance.PoolObject (this.gameObject);	}
}
