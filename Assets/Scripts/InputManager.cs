﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {
#if UNITY_ANDROID
    //	private Vector2 aux = Vector2.zero;

    //Vector2[] InitialPosition;
    //Vector2[] CurrentPosition;
    //Vector2[] Offset;
    public GameObject Ship;
    MageBehaviour MB;
    PlasmaShooter RS;
    GravityShooter GS;
    LaserShooter LS;
    Vector2 inputPos;
    Vector2 distance;


    int Touchinghold;
    //float holdCount = 0;
    //bool holdState = false;
    // Use this for initialization
    void Start() {
        //Ship = GameManager.instance.PlayerRef.gameObject;

    }

    // Update is called once per frame
    void Update() {
        if (!Ship) {
            Ship = GameManager.instance.PlayerRef.gameObject;
            MB = Ship.GetComponent<MageBehaviour>();

			LS = Ship.GetComponent<LaserShooter>();
            RS = Ship.GetComponent<PlasmaShooter>();
            GS = Ship.GetComponent<GravityShooter>();
        }

        if (GameManager.instance._currentState == GameManager.GameState.InPlay) {
            if (Input.touchCount == 0)
                return;

            for (int i = 0; i < Input.touchCount; i++) {

                switch (Input.touches[i].phase) {

                    case TouchPhase.Began:
                        TouchBegan(Input.touches[i], i);
                        break;
                    case TouchPhase.Moved:
                        TouchMoved(Input.touches[i], i);
                        break;
                    case TouchPhase.Ended:
                        TouchEnded(Input.touches[i], i);
                        break;
                    case TouchPhase.Stationary:
                        break;


                }

            }
        }

    }


    void TouchBegan(Touch input, int inputnum) {
        Vector2 wPos = Camera.main.ScreenToWorldPoint(input.position);
        RaycastHit2D hit;
        hit = Physics2D.Raycast(wPos, Vector2.zero, Mathf.Infinity);

        if (MB.Weapon == AttackType.Laser)
            LS.LaserEm();

        if (hit) {

           // Debug.Log(hit.collider.name);
            Touching(hit.collider.gameObject.tag, hit.collider.gameObject, wPos, inputnum);

        }

    }

    void TouchMoved(Touch input, int inputnum) {
        Vector2 wPos = Camera.main.ScreenToWorldPoint(input.position);
        RaycastHit2D hit;
        hit = Physics2D.Raycast(wPos, Vector2.zero, Mathf.Infinity);



        if (GS.ChargingGravity && inputnum == Touchinghold) {
            distance = wPos - inputPos;
           if(distance.magnitude != 0f)  Debug.Log(distance.magnitude);
            GS.ChargingMobile(distance);

            inputPos = wPos;
        }

        if (hit) {
          //  Debug.Log(hit.collider.name);
            Click(hit.collider.gameObject.tag, hit.collider.gameObject, wPos);
        }
    }

    void TouchEnded(Touch input, int inputnum) {
        Vector2 wPos = Camera.main.ScreenToWorldPoint(input.position);
        RaycastHit2D hit;
        hit = Physics2D.Raycast(wPos, Vector2.zero, Mathf.Infinity);

        if (hit) {
         //   Debug.Log(hit.collider.name);
            Click(hit.collider.gameObject.tag, hit.collider.gameObject, wPos);
        }

        if (inputnum == Touchinghold)
            Release(wPos);
    }


    void Touching(string name, GameObject go, Vector2 vec, int TouchHold) {


        switch (name) {
            case "Meteor":
            case "BossMeteor":
            case "Aircraft":
            case "Moon":
                //Debug.Log("Tap");
                if (MB.Weapon == AttackType.Lightning)
                    go.SendMessage("ApplyDamager", 1);
                //
                break;
            case "Mage":

                Touchinghold = TouchHold;
                if (MB.Weapon == AttackType.Plasma) {
                    RS.PlasmaEm();
                }
                if (MB.Weapon == AttackType.Gravity) {
                     GS.GravityEm();
                    inputPos = vec;
                }

                break;


        }
    }

    void Click(string name, GameObject go, Vector2 vec) {
        /*
		if(name.Equals("Meteor")||name.Equals("BossMeteor"))
		{
			//Debug.Log ("Tap");
			go.SendMessage("ApplyDamager", 1);
		}
		else if(name.Equals("SomethingElse"))
		{
			//DoSomethingElse
		}*/

        switch (name) {
            case "MoveCol":
                //Debug.Log("Mage");
                go.SendMessage("moveToX", vec.x);
                break;
        }
    }

    void Release(Vector2 vec) {
        Vector3 Touchpos = new Vector3(vec.x, vec.y, 0f);
        if (MB.Weapon == AttackType.Plasma)
            RS.Release(Touchpos);
        if (MB.Weapon == AttackType.Gravity)
           GS.Release(Touchpos);

    }
#endif
}

