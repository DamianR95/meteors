﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ModeButton : MonoBehaviour
{

    public AudioClip but;
    public Text buttonName;
    public GameObject Windows;

    public void BeamMode()
    {
        //GameManager.instance._currentMode = GameManager.GameMode.Beam;
        Windows.SetActive(false);
      //  Dynamic_Tutorial.instance.step2 = true;
        buttonName.text = "Mode: Beam";
    }

   public  void LaserMode()
    {
       // GameManager.instance._currentMode = GameManager.GameMode.Laser;
        Windows.SetActive(false);
        //Dynamic_Tutorial.instance.step2 = true;
        buttonName.text = "Mode: Laser";
    }

}