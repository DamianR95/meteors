﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContinueButton : MonoBehaviour {

	public AudioClip but;


	public void ResumeGame()
	{
		//Debug.Log("Test");
		AudioSource.PlayClipAtPoint (but, this.transform.position);

		GameManager.instance.ChangeState(GameManager.instance._laststate);
	}
}
