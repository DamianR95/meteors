﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RetryButton : MonoBehaviour {

	public AudioClip but;


	public void RetryGame()
	{
		AudioSource.PlayClipAtPoint (but, this.transform.position);
		//Application.LoadLevel (Application.loadedLevelName);
        SceneManager.LoadScene(1);

	}
}
