﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseButton : MonoBehaviour {

    public AudioClip but;

    public void PauseGame()
    {
        

            AudioSource.PlayClipAtPoint(but, this.transform.position);
        
        GameManager.instance.ChangeState(GameManager.GameState.InPause);
    }
}
