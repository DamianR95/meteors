﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ExitButton : MonoBehaviour {

	public AudioClip but;


	public void GoBack()
	{
		AudioSource.PlayClipAtPoint (but, this.transform.position);
		//Application.LoadLevel ("Menu");
        SceneManager.LoadScene(0);
		
	}
}
