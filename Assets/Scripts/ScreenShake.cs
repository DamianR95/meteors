﻿using UnityEngine;
using System.Collections;

public class ScreenShake : MonoBehaviour
{

    Vector3 originalPosition;
     public float parameter;
     public static ScreenShake instance = null;

    float shakeAmt = 0;

    //public Camera mainCamera;

    public void ShakeIt() {
        shakeAmt = 1 * .0025f;
        InvokeRepeating("CameraShake", 0, .01f);
        Invoke("StopShaking", 0.3f);
    
    
    }


    /*void OnCollisionEnter2D(Collision2D coll)
    {
        Debug.Log(coll.gameObject.name);
        shakeAmt = coll.relativeVelocity.magnitude * .0025f;
        InvokeRepeating("CameraShake", 0, .01f);
        Invoke("StopShaking", 0.3f);

    }*/

    void Awake()
    {
        instance = this;
    }


    void Start() {
        //if (!mainCamera)
       // {
            //mainCamera = Camera.main;
            originalPosition = transform.position;
       // }
    }
    void CameraShake()
    {
        if (shakeAmt > 0)
        {
            float quakeAmt = Random.value * shakeAmt * parameter - shakeAmt;
            Vector3 pp = transform.position;
            pp.x += quakeAmt; // can also add to x and/or z
            pp.y += quakeAmt; // can also add to x and/or z
            transform.position = pp;
        }
    }

    void StopShaking()
    {
        CancelInvoke("CameraShake");
       transform.position = originalPosition;
    }

}