﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tutorial_Behaviour : MonoBehaviour {

	int TutoNumber = 0;
	public GameObject[] TutorialPanel;


	public static Tutorial_Behaviour instance = null;

	public string Continue(string tutomessage){

		if (tutomessage == "Moved" && TutoNumber == 0)
			Part2 ();

		if (tutomessage == "Destroyed" && TutoNumber == 1)
			Part3 ();

		if (tutomessage == "Grabed" && TutoNumber == 2)
			Part4 ();

		if (tutomessage == "Pressed" && TutoNumber == 3)
			Part5 ();

		return "ERROR";
	}
	


	void Part2(){
		TutoNumber++;
		TutorialPanel [0].SetActive (false);
		//Mata ese meteoro
		TutorialPanel [1].SetActive (true);
		SpawnManager.instance.Spawn ("Meteor");

		Invoke ("TimeStop", 3.5f);
	}

	void Part3(){
		TutoNumber++;
		TutorialPanel [1].SetActive (false);
		Time.timeScale=1;
		
		
		//Agarra ese power up
		TutorialPanel [2].SetActive (true);
		SpawnManager.instance.Spawn ("PowerUp");
	
	}

	void Part4(){
		TutoNumber++;
		TutorialPanel [2].SetActive (false);
		
		
		//Toca OK
		TutorialPanel [3].SetActive (true);
		//Debug.Log ("Termine 4");
		
	}
	void Part5(){
		TutoNumber++;
		TutorialPanel [3].SetActive (false);
		
		TutorialPanel [4].SetActive (true);
		Time.timeScale = 0;

		//Debug.Log ("Termine 5");
	
}
	void TimeStop(){
		if(TutoNumber==1)
		Time.timeScale = 0;
	}

// Use this for initialization
	void Start () {
		//TutorialPanel = new GameObject[TutoNumber];

		//Move el Mago

		TutorialPanel [0].SetActive(true);
	}
	
	// Update is called once per frame
	void Awake () {
		instance = this;
	}

	public static bool isTutorial(){
		return instance != null;
	}
}
