﻿using UnityEngine;
using System.Collections;

public class Dynamic_Tutorial : MonoBehaviour {



    public GameObject handsTutorial;
   // public GameObject crosshair;
   // public GameObject Ship;
    //public float movethreshold;
    public static Dynamic_Tutorial instance = null;
    public bool step1 = false;
    //public bool step2 = false;
    // Use this for initialization
    void Start () {
	
	}


    void FixedUpdate() {
        /*  if (Ship == null)
              Ship = GameManager.instance.PlayerRef.gameObject;*/
#if UNITY_ANDROID
        if (Mathf.Abs(Input.acceleration.x) > 0.2f && !step1) {
            Step1();
        }
#endif

    }


   public  void Step1() {
        step1 = true;
        handsTutorial.GetComponent<Animation>().Play("FadeOut");
        Invoke("DesactiveThis", 0.5f);
		SpawnManager.instance.Spawn("Meteor_Tuto");
    }

    void Awake()
    {
        instance = this;
    }

    void DesactiveThis() {

        handsTutorial.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
	
	}
}
