﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Buyable {
    public BuyableId id;
    public int Cost;
    public bool Bought;
    
    public Buyable(BuyableId id, int Cost, bool Bought) {
        this.id = id;
        this.Cost = Cost;
        this.Bought = Bought;
    }

}

public enum BuyableId {
    MAGE_NORMAL,
    MAGE_WITCH,
    MAGE_GIRL,
    MAGE_CAT,
    NOT_IMPLEMENTED
}