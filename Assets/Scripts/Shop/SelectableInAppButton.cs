﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectableInAppButton : MonoBehaviour {

    public BuyableId id;
    private Button bt;

    public Color SelectableColor;
    private Color NoSelectableColor;

    
	// Use this for initialization
	void Start () {
        bt = this.GetComponent<Button>();
        bt.onClick.AddListener(() => { BuyableManager.GetInstance().setActiveSprite(this.id); });
        NoSelectableColor = bt.colors.disabledColor;
    }
	
	// Update is called once per frame
	void Update () {
        if (bt == null)
            bt = this.GetComponent<Button>();
        ColorBlock colors = bt.colors;
        
        if (!BuyableManager.GetInstance().CanBuy(this.id)) {
            if (BuyableManager.GetInstance().isActiveSprite(this.id)) {
                bt.interactable = false;
                colors.disabledColor = SelectableColor;
            } else {
                bt.interactable = true;
                colors.disabledColor = NoSelectableColor;
            }

        } else {
            bt.interactable = false;
            colors.disabledColor = NoSelectableColor;
        }

        bt.colors = colors;
	}
}
