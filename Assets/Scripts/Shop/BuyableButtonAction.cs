﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Analytics;

public class BuyableButtonAction : MonoBehaviour {
    public BuyableId id;
    public Text output;

    private Button btn = null;
	// Use this for initialization
	void Start () {
        if (btn == null)
            btn = this.GetComponent<Button>();

        btn.onClick.AddListener(makePurchase);
	}
	
	// Update is called once per frame
	void Update () {
        if (btn == null)
            btn = GetComponent<Button>();

        btn.interactable = BuyableManager.GetInstance().CanBuy(this.id);
	}

    void CleanText() {
        output.text = "";
    }

    void makePurchase() {
        if (!BuyableManager.GetInstance().buyObject(this.id)) {
            output.text = "Error, compra no permitida";
        } else {
            output.text = "Compra realizada con exito";
			Analytics.Transaction (id.ToString(), 0.99m, "USD", null, null);
        }
        CancelInvoke("CleanText");
        Invoke("CleanText", 1f);
    }
}
