﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using MiniJSON;

[System.Serializable]
public class BuyableManager : MonoBehaviour {

    public List<Buyable> buyables;
    private Dictionary<string, System.Object> buyableDictionary;

    private static BuyableManager instance = null;

    /*
        Data from/to load or save file information, such as... Buyables.
     */

    public string token = "87859b6921509e0Au9sjR4ep8H9T1FED0g2JH65E";
    public string saveFile = "Meteors.txt";
    public string NameTag = "Dictionary";
    public bool EncryptData = true;

    public GameObject TutorialPanel;



    public bool DestroySaveOnInit = false;
    public bool EnableBuysOnDebugger = true;

    public static BuyableManager GetInstance() {
        return instance;
    }

    void Start () {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
            LoadFromDisk();
        } else {
            Destroy(this.gameObject);
        }
	}

    string calculateString(string tag) {
        string str = "";
        str += saveFile + "?tag=";
        str += tag;

        if (EncryptData) {
            str += "&encrypt=true&password=";
            str += token;
        }

        return str;
    }

    private static string Base64Encode(string plainText) {
        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        return System.Convert.ToBase64String(plainTextBytes);
    }

    private static string Base64Decode(string base64EncodedData) {
        var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
        return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
    }

    void LoadFromDisk() {

        if (DestroySaveOnInit) {
            Debug.Log("For debug... we are destroying the save.");
            ES2.Delete(saveFile);
            FirstTimeSetup();
            return;
        }

        if (ES2.Exists(saveFile)) {
            string load = ES2.Load<string>(calculateString(NameTag));
            string decoded = Base64Decode(load);

            Debug.Log(decoded);

            buyableDictionary = Json.Deserialize(decoded) as Dictionary<string, System.Object>; 
            if (decoded == null || decoded == "null") {
                ES2.Delete(saveFile);
                Debug.LogError("Save appears to be corrupted. Deleting it.");
                FirstTimeSetup();
            } else {
                DoBuyableInitialization();
            }
        } else {
            FirstTimeSetup();
        }
    }

    void DoBuyableInitialization() {
        foreach(Buyable item in buyables) {
            if (buyableDictionary.ContainsKey(item.id.ToString())) {
                item.Bought = (bool)buyableDictionary[item.id.ToString()];
            }
        }
    }

    void SaveToDisk() {
        string s = Json.Serialize(buyableDictionary);
        Debug.Log(s);

        string encodedString = Base64Encode(s);
        ES2.Save(encodedString, calculateString(NameTag));
    }

    public virtual void FirstTimeSetup() {
        buyableDictionary = new Dictionary<string, System.Object>();
        foreach(Buyable item in buyables) {
                buyableDictionary[item.id.ToString()] = item.Bought;
        }

        buyableDictionary["Active"] = BuyableId.MAGE_NORMAL;

        //TutorialPanel.SetActive(true);
    }

    void OnApplicationQuit() {
        SaveToDisk();
    }


    public virtual bool CanBuy(BuyableId id) {
        if (buyableDictionary == null)
            return false;

        if (!buyableDictionary.ContainsKey(id.ToString()))
            return false;


        return !(bool)buyableDictionary[id.ToString()];
    }


    public virtual bool buyObject(BuyableId id) {

        if (buyableDictionary == null)
            return false;
        
        bool response = EnableBuysOnDebugger;



        if (Application.isMobilePlatform) {
        #if UNITY_ANDROID
            using (AndroidJavaClass ajo = new AndroidJavaClass("com.ligool.plugin.Main")) {
               response = ajo.CallStatic<bool>("buyCoins");  
            }     
        #endif
        }

        buyableDictionary[id.ToString()] = response;

        return response;

    }


    public virtual bool isActiveSprite(BuyableId id) {
        if (buyableDictionary == null)
            return false;
        
        return buyableDictionary["Active"].ToString() ==  id.ToString();
    }

    public virtual bool setActiveSprite(BuyableId id) {
        if (buyableDictionary == null)
            return false;

        buyableDictionary["Active"] = id.ToString();

        return true;
    }

    public virtual string getActiveSprite() {
        return buyableDictionary["Active"].ToString();
    }
}
