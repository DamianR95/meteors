﻿using GoogleMobileAds.Api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {


    //Game State

    public enum GameState { InPlay, InPause, InPerks, Results };
    //public enum GameMode { Empty, Beam, Laser};

    public GameObject GamePanel;
    public GameObject PausePanel;
    public GameObject PerkPanel;
    public GameObject ResultsPanel;

    //SOUND

    public AudioSource BGM;
    public AudioSource BossBGM;
    public AudioClip NormalTheme;
    public AudioClip ShowerTheme;



    public GameState _currentState = GameState.InPlay;
    public GameState _laststate = GameState.InPlay;
    //public GameMode _currentMode = GameMode.Empty;

    //Variables Stats
    public int MeteorCount = 0;
    int MaxChain;
    public int CurrentScore = 0;
    public int CurrentCombo = 0;
    public int CurrentCoins = 0;


    //GUI
    public Text CoinText;

    public Text ResultText;
    public Text ChainResultText;

    public Text ComboTitle;
    public Text ComboText;
    public Text ChainText;
    public Text MaxChainText;
    public Text MultiplyComboText;

    public GameObject EndCombo;

    bool MercyTime;
    public int StardustLevel = 1;

    //Combo Timer
    public Slider TimerSlider;
    public float TimerSpeed;
    //float BaseTimerSpeed = 0.01f;
    public int MultiplyCombo;
    float ComboRate = 1f;



    //SDL GUI
    public Color FColor;
    public Color LColor;

    public float NextSDLvl = 10f;
    //Recuerda agregar dos ceros!  10 = 1000
    public const int SDLvl1 = 0;
    public const int SDLvl2 = 20;
    public const int SDLvl3 = 500;
    public const int SDLvl4 = 1500;
    public const int SDLvl5 = 3000;
    public const int SDLvl6 = 5000;
    public const int SDLvl7 = 7000;
    public const int SDLvl8 = 9000;

    //HUD

    public Text LivesText;
    public float LivesCount = 3;
    public Slider LivesSlider;

    public float rateOnTime = 1;

    //DynamicTutorial
    public bool GameStarted = false;
    //public GameObject handsTutorial;
    //public GameObject crosshair;

    //Events
    // float targetScore = 250;
    float ScoreToBoss = 75;
    bool BossFight = false;

    bool ShowerLvl = false;
    float ShowerRate = 0.5f;

    //ADS

    BannerView bv = null;

    // ANALITICS






    public void toggleBGM() {
        if (!BGM)
            return;

        if (BGM.isPlaying) {
            BGM.Pause();
            BossBGM.Play();
            BossFight = true;
        } else {
            BossBGM.Stop();
            BGM.UnPause();
            BossFight = false;
        }
    }

    //

    public void LosingLife(int damage, bool toPlayer = false) {

        //CurrentScore = CurrentScore + CurrentCombo * MultiplyCombo;
        if (CurrentCombo >= 10)
            EndCombo.BroadcastMessage("PlayThis", CurrentCombo);
        CurrentCombo = 0;
        for (int i = 0; i < MultiplyCombo; i++) {
            NewMultiply(false);
        }
        //CurrentScore = 0;
        //StardustLevel = 1;
        //ChainStardustLevel ();
        //ChainText.GetComponent<Animation>().Play("Vibrate");
        LivesSlider.gameObject.SetActive(true);
        if (toPlayer) {
            if (player.ReceiveDamage())
                LivesCount -= damage;
        } else
            LivesCount -= damage;
    }

    public void NewChainRecord() {
        CurrentScore = CurrentScore + 1 * MultiplyCombo;
        CurrentCombo++;
        // TimerSlider.value += 0.08f;
        SpawnManager.instance.MessageSpawn("Output", ComboText.transform.position);
        SpawnManager.instance.MessageSpawn("Output_Chain", player.transform.position);
        ChainText.GetComponent<Animation>().Play("HudCombo");
        ComboText.GetComponent<Animation>().Play("HudCombo");
        if (CurrentScore > MaxChain) {
            MaxChain = CurrentScore;
        }
        if (CurrentScore > NextSDLvl) {
            StardustLevel++;
            ChainStardustLevel();
        }

        MercyTime = true;
        CancelInvoke("TimeIsRunning");
        Invoke("TimeIsRunning", 0.3f);

    }


    public void RisingMeteorCount() {
        NewChainRecord();
        MeteorCount++;
        TimerSlider.value += 0.08f;
        if (rateOnTime >= 0.45) { //El tope es 366 con este calculo
            rateOnTime = rateOnTime - 0.0015f;
        }
    }



    public void ChainStardustLevel() {





        switch (StardustLevel) {

            case 1:
                StardustLevel = 1;
                FColor = Color.white;
                LColor = Color.yellow;
                NextSDLvl = SDLvl2;
                AnalyticsSDL(StardustLevel);
                break;
            case 2:
                StardustLevel = 2;
                FColor = Color.yellow;
                LColor = Color.magenta;
                NextSDLvl = SDLvl3;
                AnalyticsSDL(StardustLevel);
                break;
            case 3:
                StardustLevel = 3;
                FColor = Color.magenta;
                LColor = Color.green;
                NextSDLvl = SDLvl4;
                AnalyticsSDL(StardustLevel);
                break;
            case 4:
                StardustLevel = 4;
                FColor = Color.green;
                LColor = Color.red;
                NextSDLvl = SDLvl5;
                AnalyticsSDL(StardustLevel);
                break;
            case 5:
                StardustLevel = 5;
                FColor = Color.red;
                LColor = Color.cyan;
                NextSDLvl = SDLvl6;
                AnalyticsSDL(StardustLevel);
                break;
            case 6:
                StardustLevel = 6;
                FColor = Color.cyan;
                LColor = Color.cyan;
                NextSDLvl = SDLvl7;
                AnalyticsSDL(StardustLevel);
                break;
            case 7:
                StardustLevel = 7;
                FColor = Color.cyan;
                LColor = Color.cyan;
                MeteorShowerEvent(true);

                NextSDLvl = SDLvl8;
                AnalyticsSDL(StardustLevel);
                break;
            case 8:
                StardustLevel = 8;
                FColor = Color.cyan;
                LColor = Color.cyan;
                MeteorShowerEvent(false);
                NextSDLvl = Mathf.Infinity;
                AnalyticsSDL(StardustLevel);
                break;
        }

    }

    void NewMultiply(bool Level) {

        if (MultiplyCombo == 1 && Level == false)
            return;

        TimerSlider.GetComponent<Animation>().Play("FlashSlider");
        MultiplyComboText.GetComponent<Animation>().Play("Multiply");
        if (Level == true) {
            MultiplyCombo++;
            TimerSpeed *= 1.2f;
            TimerSlider.value = 0.3f;

            if (ComboRate > 0.75f)
                ComboRate -= 0.025f;
        } else {
            MultiplyCombo--;
            TimerSpeed /= 1.2f;
            TimerSlider.value = 0.8f;
            ComboRate += 0.05f;
        }

        if (MultiplyCombo >= 10) {

            TimerSlider.GetComponent<Animation>().Play("ComboSlider");
            // ComboTitle.GetComponent<Animation>().Play("LetterFlash");
        } else
            TimerSlider.GetComponent<Animation>().Stop("ComboSlider");

    }

    void TimeIsRunning() {
        MercyTime = false;
    }

    void MeteorShowerEvent(bool IsOn) {

        if (IsOn) {
            BGM.Stop();
            BGM.clip = ShowerTheme;
            BGM.Play();
            if (BossFight) BGM.Pause();
            ShowerRate = 0.3f;
            ShowerLvl = true;
        } else {
            BGM.Stop();
            BGM.clip = NormalTheme;
            BGM.Play();
            if (BossFight) BGM.Pause();

            ShowerRate = 1f;
            ShowerLvl = false;
        }



    }


    void AnalyticsSDL(int level) {
        /*  Analytics.CustomEvent("SDLevelUp", new Dictionary<string, object>
                  {
                      { "Level:",level },
                      { "Lives:",LivesCount },
                  });*/
    }

    //SPAWNER
    //    Vector3 Spawnpos = Vector3.zero;
    [SerializeField]
    //Normal Meteor
    public float spawnRate = 3f;
    private float counter = 0;
    //Big Meteor
    public float bigspawnRate = 5f;
    private float bigcounter = 0;
    //Broken Meteor
    public float brokenspawnRate = 5f;
    private float brokencounter = 0;
    //AirCraft
    public float acraftspawnRate = 5f;
    private float acraftcounter = 0;
    //Shield Aircraft
    public float s_acraftspawnRate = 10f;
    private float s_acraftcounter = 0;

    //Power Up
    public float powerspawnRate = 5f;
    //  private float powercounter = 0;

    private MageBehaviour player = null;
    public GameObject playerPrefab = null;

    public Vector3 SpawningPoint = Vector3.zero;
    private SpriteRenderer PlayerSprite;
    public Sprite MageNormal;
    public Sprite MageWitch;
    public Sprite MageGirl;
    public Sprite MageCat;

    /* [SerializeField]
     public GameObject goInScene = null;
     [SerializeField]
  private GameObject guiInScene = null;*/

    public static GameManager instance = null;


    public MageBehaviour PlayerRef {
        get {
            return this.player;
        }
    }

    // Use this for initialization
    void Start() {

        instance = this;
        Time.timeScale = 1;
        counter = spawnRate;
        //bigcounter = bigspawnRate;
        //powercounter = powerspawnRate;
        // MageSprites = new Sprite[BuyableId.GetNames(typeof(BuyableId)).Length-1];
        FColor = Color.white;
        LColor = Color.yellow;

        GameObject go = Instantiate(playerPrefab, SpawningPoint, Quaternion.identity) as GameObject;
        go.transform.parent = SpawnManager.instance.goInScene.transform;

        //set parent


        player = go.GetComponent<MageBehaviour>();

        PlayerSprite = go.GetComponent<SpriteRenderer>();

        if (!BuyableManager.GetInstance()) {
            Debug.LogWarning("You shouldnt start the game without this Object...");
        } else {
            switch (BuyableManager.GetInstance().getActiveSprite()) {
                case "MAGE_NORMAL":
                    PlayerSprite.sprite = MageNormal;
                    break;
                case "MAGE_WITCH":
                    PlayerSprite.sprite = MageWitch;
                    break;
                case "MAGE_GIRL":
                    PlayerSprite.sprite = MageGirl;
                    break;
                case "MAGE_CAT":
                    PlayerSprite.sprite = MageCat;
                    break;
            }
        }


        //goInScene = GameObject.FindGameObjectWithTag("SceneObjectsInCanvas") as GameObject;

        //ImportAds();

        _currentState = GameState.InPlay;

    }

    void ImportAds() {
        AdSize newSize = new AdSize(250, 40);
        bv = new BannerView(
        "ca-app-pub-6612439843804869/1211048835", newSize, AdPosition.BottomRight);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bv.LoadAd(request);
    }

    public Vector2[] GetVectorFromCameraBound() {
        Vector2[] elements = new Vector2[2];


        Vector2 min = Vector2.zero;
        min.x = 0.05f;
        min = Camera.main.ViewportToWorldPoint(min);

        Vector2 max = Vector2.one;
        max.x = 0.95f;
        max = Camera.main.ViewportToWorldPoint(max);

        elements[0] = min;
        elements[1] = max;

        return elements;
    }

    void Update() {

        //if(Tutorial_Behaviour.isTutorial()
        //Spawner de Meteors
        counter += Time.deltaTime;
        if (counter >= spawnRate && GameStarted) {
            counter = 0;
            SpawnManager.instance.Spawn("Meteor");
            spawnRate = Random.Range(1.5f * rateOnTime * ComboRate, 2.5f * rateOnTime * ComboRate * ShowerRate);
        }
        //Spawner de BigMeteors
        if (!ShowerLvl && !BossFight)
            bigcounter += Time.deltaTime;

        if (bigcounter >= bigspawnRate && StardustLevel >= 2) {
            bigcounter = 0;
            SpawnManager.instance.Spawn("BigMeteor");
            bigspawnRate = Random.Range(15.5f * rateOnTime, 20.5f * rateOnTime);
        }
        //Spawner de BrokenMeteors
        if (!ShowerLvl && !BossFight)
            brokencounter += Time.deltaTime;
        if (brokencounter >= bigspawnRate && StardustLevel >= 3) {
            brokencounter = 0;
            SpawnManager.instance.Spawn("BrokenMeteor");
            brokenspawnRate = Random.Range(15.5f * rateOnTime, 20.5f * rateOnTime);
        }

        //Spawner de Aircraft
        if (!ShowerLvl && !BossFight)
            acraftcounter += Time.deltaTime;
        if (acraftcounter >= acraftspawnRate && StardustLevel >= 4) {
            acraftcounter = 0;
            SpawnManager.instance.SideSpawn("AirCraft");
            acraftspawnRate = Random.Range(20.5f * rateOnTime, 25.5f * rateOnTime);
        }


        //Spawner de Shield Aircraft
        if (!ShowerLvl && !BossFight)
            s_acraftcounter += Time.deltaTime;
        if (s_acraftcounter >= s_acraftspawnRate && StardustLevel >= 6) {
            s_acraftcounter = 0;
            SpawnManager.instance.SideSpawn("Shield_AirCraft");
            s_acraftspawnRate = Random.Range(30.5f * rateOnTime, 40.5f * rateOnTime);
        }


        //Spawner de PowerUp
        /*
        if(GameStarted)
        powercounter += Time.deltaTime;
        if (powercounter >= powerspawnRate) {
            powercounter = 0;
            if (!ShowerLvl )
                Spawn("PowerUp");
            else
                Spawn("GreatMeteor");
            powerspawnRate = Random.Range(13.5f, 17.5f);
        }*/
        //GUI

        CoinText.text = "Coins: " + CurrentCoins.ToString();
        LivesText.text = "Lives: " + LivesCount.ToString();
        LivesText.color = Color.Lerp(Color.red, Color.green, LivesCount / 3f);
        ComboText.text = CurrentCombo.ToString() + "!";
        ChainText.text = "Score: " + (CurrentScore * 100).ToString();
        ChainText.color = Color.Lerp(FColor, LColor, CurrentScore / NextSDLvl);
        MultiplyComboText.text = "x" + MultiplyCombo.ToString();
        // MaxChainText.text = "Max: " + MaxChain.ToString();


        if (TimerSlider.value > 0 && !MercyTime)


            TimerSlider.value -= Time.deltaTime * TimerSpeed;


        if (TimerSlider.value < 0.05f)
            NewMultiply(false);

        if (TimerSlider.value >= 0.95f)
            NewMultiply(true);


        /*  if (MeteorCount >= targetScore) {
              LivesCount++;
              targetScore += 200;
          }*/
        if (MeteorCount >= ScoreToBoss) {
            SpawnManager.instance.Spawn("BlueBody");
            GameManager.instance.toggleBGM();
            ScoreToBoss += 75;
        }

        // if(LivesCount == 1)            LivesText.GetComponent<Animation>().Play("FlashingRed");

        // if (StardustLevel == 6)
        //   ChainText.GetComponent<Animation>().PlayQueued("Vibrate");




        if (LivesCount <= 0)
            ChangeState(GameState.Results);

        if (Input.GetKeyDown(KeyCode.Escape) && _currentState != GameState.Results)
            ChangeState(GameState.InPause);


    }


    void FixedUpdate() {

        if (LivesCount > 0 && LivesCount <= 3) {

            LivesCount += 0.0003f;
            LivesSlider.value = LivesCount;
        } else {
            LivesSlider.gameObject.SetActive(false);
        }
    }



    void OnApplicationPause(bool pauseStatus) {
        if (pauseStatus && _currentState != GameState.Results)
            ChangeState(GameState.InPause);
    }

    public void ChangeState(GameState state) {

        switch (state) {
            case GameState.InPlay:
                //Debug.Log("Play");
                GamePanel.SetActive(true);
                PausePanel.SetActive(false);
                PerkPanel.SetActive(true);
                Time.timeScale = 1;
                break;

            case GameState.InPerks:
                //Debug.Log("Play");
                GamePanel.SetActive(true);
                PausePanel.SetActive(false);

                PerkPanel.SetActive(true);
                Time.timeScale = 0.3f;
                break;


            case GameState.InPause:
                //Debug.Log("Pause");
                if (Tutorial_Behaviour.isTutorial()) break;
                PausePanel.SetActive(true);
                GamePanel.SetActive(false);
                PerkPanel.SetActive(false);
                //PanelPerk.instance.PerkOn = false;

                Time.timeScale = 0;

                break;
            case GameState.Results:
                //Debug.Log("Finish");

                // allAudioSources = FindObjectsOfType(AudioSource) as AudioSource[];
                ResultsPanel.SetActive(true);
                GamePanel.SetActive(false);
                PerkPanel.SetActive(false);
                Time.timeScale = 0;
                ResultText.text = "Meteors Destroyed: " + MeteorCount.ToString();
                ChainResultText.text = "Score: " + (CurrentScore * 100).ToString();
                Analytics.CustomEvent("gameOver", new Dictionary<string, object>
                    {
                    { "Meteor Destroyed:", MeteorCount },
                    {"Max Chain:", MaxChain  },
                    {"Score:", CurrentScore*100 },
                });
                break;
        }
        _laststate = _currentState;
        _currentState = state;
    }



    string PowerDecide() {
        int PowerDecider = Random.Range(0, 3);

        switch (PowerDecider) {
            case 0:
                return "FireUp";

            case 1:
                return "IceUp";
            case 2:
                return "LightningUp";
        }
        return null;

    }


   

}
