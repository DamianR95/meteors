﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {


	public Button Play;
	public Button HowTo;
    public Button Shop;
    public Button Exitshop;
    public Button Exit;
    public GameObject menuwindow;
    public GameObject shopwindow;



	public void ClickPlay(){
		//Application.LoadLevel ("game");
        SceneManager.LoadScene(1);
        
	}

	public void ClickHowTo(){
		//Application.LoadLevel ("tutorial");
        SceneManager.LoadScene(2);
	}
    public void ClickShop() {
        shopwindow.SetActive(true);
        menuwindow.SetActive(false);
    }

    public void ClickExitShop() {
        shopwindow.SetActive(false);
        menuwindow.SetActive(true);
    }

    public void ClickExit(){

		Debug.Log ("Quit");
		Application.Quit();
	}


	public void ToggleSound(){
		if(AudioListener.pause == false){
			Debug.Log ("IsOff");
			AudioListener.pause = true;
		}
		else{
			Debug.Log ("IsOn");
			AudioListener.pause = false;
		}

	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
